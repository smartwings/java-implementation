package com.szz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @EnableDiscoveryClient 注册微服务到注册中心
 */
@EnableDiscoveryClient
@SpringBootApplication
public class SpringCloudEurekaClientProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudEurekaClientProviderApplication.class, args);
    }

}
