package com.szz.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Data 该注解快速让实体类注入get、set、toString...方法
 * @NoArgsConstructor 该注解快速让实体类注入无参构造方法
 * @AllArgsConstructor 该注解快速让实体类注入全参构造方法
 * @Builder 该注解通过构建者模式，来封装实体类对象，链式编程
 * @Accessors(chain = true) 通过注解可以链式编程封装和返回实体类对象，所有的set方法的返回值都是本身
 * @JsonInclude(JsonInclude.Include.NON_NULL) 当实体类中的属性为null时，过滤掉
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Result<T> {
    private Integer code;
    private String msg;
    private boolean success;
    private T data;
}
