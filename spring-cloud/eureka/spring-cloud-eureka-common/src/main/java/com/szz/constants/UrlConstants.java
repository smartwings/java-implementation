package com.szz.constants;

public interface UrlConstants {

    String BASE_URL = "http://localhost:2001";

    String PROVIDER_01 = BASE_URL + "/provider/01/{id}";
    String PROVIDER_02 = BASE_URL + "/provider/02/{id}?aaa=bbb";
    String PROVIDER_03 = BASE_URL + "/provider/03/{id}?name=zhangsan";
    String PROVIDER_04 = BASE_URL + "/provider/04/{id}?name=lisi";
    String PROVIDER_05 = BASE_URL + "/provider/05/{id}?name=wangwu";

}
