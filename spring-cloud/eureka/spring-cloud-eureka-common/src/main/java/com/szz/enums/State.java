package com.szz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum State {

    SUCCESS(20000, "请求成功"),
    FAILED(20001, "请求失败");

    private Integer code;
    private String msg;

}
