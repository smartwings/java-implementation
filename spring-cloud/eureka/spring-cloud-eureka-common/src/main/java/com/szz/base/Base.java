package com.szz.base;


import com.szz.enums.State;
import com.szz.utils.Result;

public class Base {

    public <T> Result ok(T data) {
        return Result.builder()
                .code(State.SUCCESS.getCode())
                .msg(State.SUCCESS.getMsg())
                .success(true)
                .data(data)
                .build();
    }

    public Result err() {
        return Result.builder()
                .code(State.FAILED.getCode())
                .msg(State.FAILED.getMsg())
                .success(false)
                .build();
    }

}
