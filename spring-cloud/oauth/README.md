# security+oauth2.0
## 使用
### 搭建授权服务器
1. pom添加依赖
    ```java
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-oauth2</artifactId>
        <version>2.2.5.RELEASE</version>
    </dependency>
    ```
2. 启动类添加注解
    - @EnableAuthorizationServer  
3. 修改yml配置文件
    ```java
    server:
        port: 9999
    spring:
        application:
            name: auth-server
        redis:  #redis的配置 用于存放我们颁发的token
            host: 192.168.226.128
            port: 6379
            database: 0
    ```
4. 创建配置类AuthorizationConfig
    ```java
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.Configuration;
    import org.springframework.data.redis.connection.RedisConnectionFactory;
    import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
    import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
    import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
    import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
    import org.springframework.security.oauth2.provider.token.TokenStore;
    import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
    import org.springframework.web.bind.annotation.RestController;
    
    import javax.annotation.Resource;
    
    @Configuration  //标记为我们自己的配置类
    public class AuthorizationConfig extends AuthorizationServerConfigurerAdapter {
        @Resource
        private BCryptPasswordEncoder passwordEncoder;
    
        //注入redis的连接工厂
        @Autowired
        private RedisConnectionFactory redisConnectionFactory;
    
    
        /**
         * 描述: 创建一个redis的tokenStore存放颁发的token
         */
        @Bean
        public TokenStore tokenStore() {
            return new RedisTokenStore(redisConnectionFactory);
        }
    
    
        /*
             第三方的账号配置信息
          */
        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            //创建一个基本的第三方应用，第三方应用少的时候，就放在内存中，多的就使用jdbc
            clients.inMemory()
                    .withClient("web")  //创建一个web第三方
                    .secret(passwordEncoder.encode("web-secret"))   //密码
                    .scopes("web-scopes")   //作用域
                    .authorizedGrantTypes("authorization_code") //授权方式一共有四种，这里是验证码授权，还有密码授权，静默授权，客户端授权
                    .redirectUris("https://www.baidu.com")   //访问成功后的跳转地址
                    .accessTokenValiditySeconds(7200);  //token的过期时间给2小时
    
    //        //在内存中配置第三方的账号信息(授权账号信息)
    //        clients.inMemory()
    //                //第三方用户名
    //                .withClient("web")
    //                //账号的密码
    //                .secret(passwordEncoder.encode("web-secret"))
    //                //token的过期时间，以秒为单位
    //                .accessTokenValiditySeconds(7200)
    //                //作用域
    //                .scopes("web-scopes")
    //                //授权方式：**验证码授权(第三方、微信)**、静默授权、**客户端授权**、**密码授权**
    //                .authorizedGrantTypes("authorization_code")//验证码授权方式
    //                //重定向的地址，用于获取令牌
    //                .redirectUris("https://www.baidu.com")
    //                .and()
    //                .withClient("ios")
    //                .accessTokenValiditySeconds(7200)
    //                .secret(passwordEncoder.encode("ios-secret"))
    //                .scopes("ios-scopes")
    //                .authorizedGrantTypes("implicit")//静默授权
    //                .redirectUris("https://www.baidu.com")
    //                .and()
    //                //密码授权方式，必须要配置认证管理器
    //                .withClient("api")
    //                .secret(passwordEncoder.encode("api-secret"))
    //                .scopes("api-scopes")
    //                .accessTokenValiditySeconds(7200)
    //                .authorizedGrantTypes("password")//密码授权
    //                .redirectUris("https://www.baidu.com")
    //                .and()
    //                .withClient("client")
    //                .secret(passwordEncoder.encode("client-secret"))
    //                .scopes("client-scopes")
    //                .accessTokenValiditySeconds(7200)
    //                .authorizedGrantTypes("client_credentials")//客户端授权
    //                .redirectUris("https://www.baidu.com")
    //        ;
        }
    
        /**
         * 描述: token的存储方式 我们先存在redis里面
         *
         * @param endpoints:
         * @return void
         */
        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            //使用redis做token的存储
            endpoints.tokenStore(tokenStore());
    
        }
    }

    ```
5. 配置WebSecurityConfig
    ```java
    package com.szz.config;
    
    import com.szz.service.impl.SysUserServiceImpl;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.Configuration;
    import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
    import org.springframework.security.config.annotation.web.builders.HttpSecurity;
    import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
    import org.springframework.security.core.authority.SimpleGrantedAuthority;
    import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
    import org.springframework.security.crypto.password.PasswordEncoder;
    
    import java.util.HashSet;
    
    @Configuration
    public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
        @Autowired
        private SysUserServiceImpl sysUserService;
    
        @Bean
        public BCryptPasswordEncoder passwordEncoder(){
            return new BCryptPasswordEncoder();
        }
    
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            //设置所有的请求都要在登陆后才能访问
            http.authorizeRequests().anyRequest().authenticated();
            //给一个登录页面
            http.formLogin();
        }
    
        /*
              认证用户的方式，指定通过数据库来进行认证(登录)
           */
        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(sysUserService);
        }
    
    
    }
   
    ```
### 静默授权(常用)
1. 修改AuthorizationConfig类
    ```java
    /**
     * 描述: 第三方应用的配置，只有配置了第三方应用，才能访问授权服务器
     *
     * @param clients:
     * @return void
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        //创建一个基本的第三方应用，第三方应用少的时候，就放在内存中，多的就使用jdbc
        clients.inMemory()
                .withClient("web")  //创建一个web第三方
                .secret(passwordEncoder.encode("web-secret"))   //密码
                .scopes("web-scopes")   //作用域
                .authorizedGrantTypes("authorization_code") //授权方式一共有四种，这里是验证码授权，还有密码授权，静默授权，客户端授权
                .redirectUris("https://www.baidu.com")   //访问成功后的跳转地址
                .accessTokenValiditySeconds(7200)  //token的过期时间给2小时
                .and()
                .withClient("ios")
                .secret(passwordEncoder.encode("ios-secret"))
                .scopes("ios-scopes")
                .authorizedGrantTypes("implicit")//静默授权
                .redirectUris("https://www.baidu.com")
                .accessTokenValiditySeconds(7200);
       
    }
    ```
   - 这种方式把令牌直接传给前端，是很不安全的。
   - 因此，只能用于一些安全要求不高的场景，并且令牌的有效期必须非常短，通常就是会话期间（session）有效，浏览器关掉，令牌就失效了
### 密码授权(重要)
1. 密码授权必须要有认证管理器，修改WebSecurityConfig
    ```java
    /**
     * 密码授权 需要这个认证管理器
     *
     * @return
     * @throws Exception
     */
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    ```
2. 修改AuthorizationConfig类
    ```java
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.Configuration;
    import org.springframework.data.redis.connection.RedisConnectionFactory;
    import org.springframework.security.authentication.AuthenticationManager;
    import org.springframework.security.crypto.password.PasswordEncoder;
    import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
    import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
    import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
    import org.springframework.security.oauth2.provider.token.TokenStore;
    import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
    
    @Configuration  //标记为我们自己的配置类
    public class AuthorizationConfig extends AuthorizationServerConfigurerAdapter {
    
    
        //注入redis的连接工厂
        @Autowired
        private RedisConnectionFactory redisConnectionFactory;
    
        /**
         * 描述: 创建一个redis的tokenStore存放颁发的token
         */
        @Bean
        public TokenStore tokenStore() {
            return new RedisTokenStore(redisConnectionFactory);
        }
    
        @Autowired
        @Lazy
        private PasswordEncoder passwordEncoder;
    
        /**
         * 描述: 注入认证管理器
         *
         * @return
         */
        @Autowired
        private AuthenticationManager authenticationManager;
    
        /**
         * 描述: 第三方应用的配置，只有配置了第三方应用，才能访问授权服务器
         *
         * @param clients:
         * @return void
         */
        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            //创建一个基本的第三方应用，第三方应用少的时候，就放在内存中，多的就使用jdbc
            clients.inMemory()
                    .withClient("web")  //创建一个web第三方
                    .secret(passwordEncoder.encode("web-secret"))   //密码
                    .scopes("web-scopes")   //作用域
                    .authorizedGrantTypes("authorization_code") //授权方式一共有四种，这里是验证码授权，还有密码授权，静默授权，客户端授权
                    .redirectUris("https://www.baidu.com")   //访问成功后的跳转地址
                    .accessTokenValiditySeconds(7200)
                    .and()
                    .withClient("ios")
                    .secret(passwordEncoder.encode("ios-secret"))
                    .scopes("ios-scopes")
                    .authorizedGrantTypes("implicit")//静默授权
                    .redirectUris("https://www.baidu.com")
                    .accessTokenValiditySeconds(7200)
                    .and()
                    .withClient("api")
                    .secret(passwordEncoder.encode("api-secret"))
                    .scopes("api-scopes")
                    .authorizedGrantTypes("password")   //密码授权
                    .redirectUris("https://www.baidu.com")
                    .accessTokenValiditySeconds(7200);  //token的过期时间给2小时
            
        }
    
        /**
         * 描述: token的存储方式 我们先存在redis里面
         *
         * @param endpoints:
         * @return void
         */
        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            //使用redis做token的存储,密码授权必须要认证管理器
            endpoints.tokenStore(tokenStore()).authenticationManager(authenticationManager);
            
        }
    }
    
    ```
### 客户端授权
1. 修改AuthorizationConfig类
    ```java
    /**
     * 描述: 第三方应用的配置，只有配置了第三方应用，才能访问授权服务器
     *
     * @param clients:
     * @return void
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        //创建一个基本的第三方应用，第三方应用少的时候，就放在内存中，多的就使用jdbc
        clients.inMemory()
                .withClient("web")  //创建一个web第三方
                .secret(passwordEncoder.encode("web-secret"))   //密码
                .scopes("web-scopes")   //作用域
                .authorizedGrantTypes("authorization_code") //授权方式一共有四种，这里是验证码授权，还有密码授权，静默授权，客户端授权
                .redirectUris("https://www.baidu.com")   //访问成功后的跳转地址
                .accessTokenValiditySeconds(7200)
                .and()
                .withClient("ios")
                .secret(passwordEncoder.encode("ios-secret"))
                .scopes("ios-scopes")
                .authorizedGrantTypes("implicit")//静默授权
                .redirectUris("https://www.baidu.com")
                .accessTokenValiditySeconds(7200)
                .and()
                .withClient("api")
                .secret(passwordEncoder.encode("api-secret"))
                .scopes("api-scopes")
                .authorizedGrantTypes("password")   //密码授权
                .redirectUris("https://www.baidu.com")
                .accessTokenValiditySeconds(7200)
                .and()
                .withClient("client")
                .secret(passwordEncoder.encode("client-secret"))
                .scopes("client-scopes")
                .authorizedGrantTypes("client_credentials")   //客户端授权
                .redirectUris("https://www.baidu.com")
                .accessTokenValiditySeconds(7200);  //token的过期时间给2小时
       
    }
    ```
### 将授权服务器变成资源服务器再试试访问【重点】
1. 修改启动类
    @EnableResourceServer   //开启资源服务器 下面所有的接口访问都需要带token
### 测试权限
1. 启动类添加注解,开启角色拦截
    - @EnableGlobalMethodSecurity(prePostEnabled = true)  //security里面的注解，所有的方法级别的访问需要验证权限