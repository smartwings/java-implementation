package com.szz.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.szz.domain.SysUser;
import com.szz.mapper.SysUserMapper;
import com.szz.service.SysUserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 *
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser>
    implements SysUserService, UserDetailsService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //1. 根据用户名查询用户信息
        SysUser sysUser = sysUserMapper.selectOne(
                new LambdaQueryWrapper<SysUser>()
                        .eq(StringUtils.isNotBlank(username), SysUser::getUsername, username)
        );

        if(!ObjectUtils.isEmpty(sysUser)){

            //根据用户的id，查询权限信息
            List<String> perCodeList = sysUserMapper.selectPerCodeByUserId(sysUser.getUserid());

            //授权操作
            sysUser.setAuths(perCodeList);

            return sysUser;

        }


        return null;
    }
}




