package com.szz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szz.domain.SysUser;


/**
 *
 */
public interface SysUserService extends IService<SysUser> {

}
