package com.szz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.szz.domain.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.powernode.springcloud.domain.SysUser
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    @Select({
            "select percode from sys_permission p",
            "left join sys_role_permission rp on p.perid = rp.perid",
            "left join sys_user_role ur on ur.userid = #{userid}"
    })
    List<String> selectPerCodeByUserId(Integer userid);
}




