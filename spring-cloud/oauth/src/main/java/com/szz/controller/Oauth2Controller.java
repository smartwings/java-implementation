package com.szz.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class Oauth2Controller {

    @GetMapping("/")
    public String hello(){
        return "Hello Oauth2...";
    }


    /*
        获取内存中认证过后的权限信息
     */
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/getUserInfo1")
    public Object getUserInfo1(Principal principal){
        return principal;
    }

    @PreAuthorize("hasRole('ROLE_TEST')")
    @GetMapping("/getUserInfo2")
    public Object getUserInfo2(){
        //通过Security的上下文对象，来获取认证用户的权限信息
        //当前返回的是认证的对象，包含：权限列表、用户名、是否认证...
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @PreAuthorize("hasAuthority('user:save')")
    @GetMapping("/user/saveUser")
    public String saveUser(){
        return "新增用户";
    }
    @PreAuthorize("hasAuthority('user:query')")
    @GetMapping("/user/queryUser")
    public String queryUser(){
        return "查询用户";
    }
    @PreAuthorize("hasAuthority('user:update')")
    @GetMapping("/user/updateUser")
    public String updateUser(){
        return "更新用户";
    }
    @PreAuthorize("hasAuthority('user:delete')")
    @GetMapping("/user/deleteUser")
    public String deleteUser(){
        return "删除用户";
    }
    @PreAuthorize("hasAuthority('goods:save')")
    @GetMapping("/goods/saveGoods")
    public String saveGoods(){
        return "新增商品";
    }
    @PreAuthorize("hasAuthority('goods:query')")
    @GetMapping("/goods/queryGoods")
    public String queryGoods(){
        return "查询商品";
    }
    @PreAuthorize("hasAuthority('goods:update')")
    @GetMapping("/goods/updateGoods")
    public String updateGoods(){
        return "更新商品";
    }
    @PreAuthorize("hasAuthority('goods:delete')")
    @GetMapping("/goods/deleteGoods")
    public String deleteGoods(){
        return "删除商品";
    }


}
