# 服务熔断
## 使用
1.feign接口注解中添加
```java
@FeignClient(value = "provider-service", fallback =
        OrderServiceHystrix.class)
```
2.创建熔断器组件
```java
@Component
public class OrderServiceHystrix implements Feign {
    @Override
    public String doOrder() {
        System.out.println("调用下单服务失败，我走 hystrix 了");
        return "我是 hystrix 的 doOrder，说明下单失败了";
    }
}
```
