package com.szz.hystrix;

import com.szz.feign.Feign;
import org.springframework.stereotype.Component;

@Component
public class OrderServiceHystrix implements Feign {
    @Override
    public String doOrder() {
        System.out.println("调用下单服务失败，我走 hystrix 了");
        return "我是 hystrix 的 doOrder，说明下单失败了";
    }
}
