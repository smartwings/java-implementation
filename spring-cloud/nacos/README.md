# 注册中心
## 使用
1. 启动nacos
2. 客户端进行配置
```yml
server:
  port: 8080
spring:
  application:
    name: nacos-client-a
  cloud:
    nacos: # 如果不指定命名空间会默认注册到public里面去 如果没有指定分组 会注册到DEFAULT_GROUP
      server-addr: localhost:8848 # 往这个地址去注册自己
      username: nacos
      password: nacos
      discovery:
        namespace: a3425a5d-9074-4925-8d55-e43fd64d38d0
        group: A_GROUP
```
3.pom.xml
```xml
<properties>
    <java.version>1.8</java.version>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <!--需要版本对应-->
    <spring-boot.version>2.3.12.RELEASE</spring-boot.version>
    <spring-cloud.version>Hoxton.SR12</spring-cloud.version>
    <spring-cloud-alibaba.version>2.2.7.RELEASE</spring-cloud-alibaba.version>
</properties>

<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <!--nacos依赖-->
    <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
    </dependency>
    <!--集成openfeign-->
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-openfeign</artifactId>
    </dependency>

</dependencies>

<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-dependencies</artifactId>
            <version>${spring-boot.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-alibaba-dependencies</artifactId>
            <version>${spring-cloud-alibaba.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-dependencies</artifactId>
            <version>${spring-cloud.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```
4.启动类添加注解
- @EnableDiscoveryClient  // 开启服务发现客户端
### 集成openfeign
1. 添加feign接口
```java
//服务名需要与远程名称一致
@FeignClient(value = "user-service")
public interface TestFeign {

    @GetMapping("info")
    public String info();

}
```
2.启动类添加注解
- @EnableFeignClients
# 配置中心
## 使用
1. 可以给实体类添加注解来获取nacos中配置的数据
    - 类上添加@RefreshScope  刷新作用域
    - @Value("${hero.name}")
2. 添加bootstrap.yml进行配置
```yml
server:
    port: 8083
spring:
    application:
        name: nacos-config-a
# 项目在启动的时候去哪里找它对应的配置文件呢??
    cloud:
        nacos:
            config:
                server-addr: localhost:8848
                username: nacos
                password: nacos
                namespace: a3425a5d-9074-4925-8d55-e43fd64d38d0 # 目前读取多配置文件的方式只支持在同一个命名空间下
                prefix: config-clint-a  # 读哪个配置文件 默认用的是应用名称  是可以修改的
                file-extension: yml  # 文件类型
    profiles:
        active: dev
```