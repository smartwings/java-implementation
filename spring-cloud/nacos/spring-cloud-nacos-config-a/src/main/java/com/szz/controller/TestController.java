package com.szz.controller;


import com.szz.model.Hero;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    public Hero hero;

    @GetMapping("info")
    public String heroInfo() {
        return hero.getName() + ":" + hero.getAge() + ":" + hero.getAddress();
    }

}
