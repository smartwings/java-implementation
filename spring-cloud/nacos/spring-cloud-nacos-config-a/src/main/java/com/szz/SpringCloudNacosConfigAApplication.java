package com.szz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudNacosConfigAApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudNacosConfigAApplication.class, args);
    }

}
