package com.szz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SpringCloudNacosClientBApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudNacosClientBApplication.class, args);
    }

}
