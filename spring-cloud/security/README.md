# 认证与鉴权
## 入门
1. 启动类添加注解
    - @EnableWebSecurity
2. 添加依赖
    ```xml
       <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>
    ```
3. 自定义密码
    ```yml
    spring:
        security:
            user:
                name: admin         #默认使用的用户名
                password: 123456    #默认使用的密码
    ```
## 配置多用户认证
1. 创建认证的配置类
    ```java
    @Configuration
    public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
        /**
         * 配置用户信息，模拟内存用户数据
         *
         * @param auth
         * @throws Exception
         */
        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            // 在内存中配置两个用户
            auth.inMemoryAuthentication()
                    .withUser("cxs")
                    .password(passwordEncoder().encode("123"))
                    .roles("ADMIN")
                    .and()
                    .withUser("test")
                    .password(passwordEncoder().encode("123"))
                    .roles("TEST");
        }
        /*
         * 从 Spring5 开始，强制要求密码要加密
         * 如果非不想加密，可以使用一个过期的 PasswordEncoder 的实例 NoOpPasswordEncoder，
         * 但是不建议这么做，毕竟不安全。
         *
         * @return
         */
        @Bean
        public BCryptPasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder();
        }
    }
    ```
## 如何获取当前登录用户的信息（两种方式）
1. UserController
    ```java
     /**
     * 获取当前用户信息，直接在参数中注入Principal对象
     * 此对象是登录后自动写入UsernamePasswordAuthenticationToken类中
     *
     * @param principal
     * @return
     */
    @GetMapping("userInfo")
    public Principal getUserInfo(Principal principal) {
        return principal;
    }
    /**
     * SecurityContextHolder.getContext()获取安全上下文对象
     * 就是那个保存在 ThreadLocal 里面的安全上下文对象
     * 总是不为null(如果不存在，则创建一个authentication属性为null的empty安全上下文对象)
     * 获取当前认证了的 principal(当事人),或者 request token (令牌)
     * 如果没有认证，会是 null,该例子是认证之后的情况
     */
    @GetMapping("userInfo2")
    public Object getUserInfo2() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication;
    }
   
    ```
## Spring Security用户，角色，权限拦截配置
1. 添加权限
    配置用户时添加
    .authorities("sys:save", "sys:del", "sys:update", "sys:query") 
2. 配置http请求验证
    ```java
     /**
         * 配置http请求验证等
         *
         * @param http
         * @throws Exception
         */
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // 注释掉他自己的方法 走我们自己的
            // super.configure(http);
            // 给一个表单登陆 就是我们的登录页面,登录成功或者失败后走我们的url
            http.formLogin().successForwardUrl("/welcome").failureForwardUrl("/fail");
            // 匹配哪些url，需要哪些权限才可以访问 当然我们也可以使用链式编程的方式
            http.authorizeRequests()
                    .antMatchers("/query").hasAnyAuthority("sys:query")
                    .antMatchers("/save").hasAnyAuthority("sys:save")
                    .antMatchers("/del").hasAnyAuthority("sys:del")
                    .antMatchers("/update").hasAnyAuthority("sys:update")
                    .antMatchers("/admin/**").hasRole("ADMIN")
                    .anyRequest().authenticated(); // 其他所有的请求都需要登录才能进行
        }
    ```
## Spring Security方法级别的授权（鉴权）
1. 在WebSecurityConfig类上添加注解
    - @EnableGlobalMethodSecurity(prePostEnabled = true)
2. 注解使用
    - @PreAuthorize  在方法调用前进行权限检查
    - @PostAuthorize 在方法调用后进行权限检查
## Spring Security返回JSON（前后端分离）
1. 创建处理器RestAuthorizationAccessDeniedHandler
    ```java
    @Configuration
    public class RestAuthorizationAccessDeniedHandler implements AccessDeniedHandler {
    
        @Override
        public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
            // 设置数据类型
            response.setContentType("application/json;charset=utf-8");
            // 封装数据
            Map<String, Object> data = new HashMap<>(4);
            data.put("code", "403");
            data.put("msg", "您没有访问权限");
            // 拿到响应流
            PrintWriter writer = response.getWriter();
            // 创建类型转换器
            ObjectMapper objectMapper = new ObjectMapper();
            String s = objectMapper.writeValueAsString(data);
            // 将数据写出去
            writer.write(s);
            writer.flush();
            writer.close();
        }
    }
    ```
2. 修改WebSecurityConfig出现拒接访问走自己的处理器
    ```java
     /**
         * 将自定义的拒绝访问处理器注入进来
         */
        @Autowired
        private AccessDeniedHandler accessDeniedHandler;

        /**
         * 配置http请求验证等
         *
         * @param http
         * @throws Exception
         */
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // 注释掉他自己的方法 走我们自己的
            // super.configure(http);
            // 给一个异常处理器，走我们自定义的拒绝访问处理器
            http.exceptionHandling().accessDeniedHandler(accessDeniedHandler);
    
            // 给一个表单登陆 就是我们的登录页面,登录成功或者失败后走我们的url
            http.formLogin().successForwardUrl("/welcome").failureForwardUrl("/fail");
            // 匹配哪些url，需要哪些权限才可以访问 当然我们也可以使用链式编程的方式
            http.authorizeRequests()
    //                .antMatchers("/query").hasAnyAuthority("sys:query")
    //                .antMatchers("/save").hasAnyAuthority("sys:save")
    //                .antMatchers("/del").hasAnyAuthority("sys:del")
    //                .antMatchers("/update").hasAnyAuthority("sys:update")
    //                .antMatchers("/admin/**").hasRole("ADMIN")
                    .anyRequest().authenticated(); // 其他所有的请求都需要登录才能进行
        }
    ```
3. 登录成功或者失败都返回JSON，我们需要自定义处理器
    登录成功返回json的处理器
    ```java
    @Configuration
    public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    
        @Override
        public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
            response.setContentType("application/json;charset=utf-8");
            PrintWriter pw = response.getWriter();
            Map<String, Object> map = new HashMap<>();
            map.put("code", 200);
            // 将用户信息放进去
            map.put("msg", authentication.getPrincipal());
            pw.write(new ObjectMapper().writeValueAsString(map));
            pw.flush();
            pw.close();
        }
    }
    ```
    登录失败返回json的处理器
    ```java
    @Configuration
    public class MyAuthenticationFailureHandler implements AuthenticationFailureHandler {
    
        @Override
        public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
            response.setContentType("application/json;charset=utf-8");
            PrintWriter pw = response.getWriter();
            Map<String, Object> map = new HashMap<>();
            map.put("code", 401);
            if (exception instanceof LockedException) {
                map.put("msg", "账户被锁定，登陆失败！");
            } else if (exception instanceof BadCredentialsException) {
                map.put("msg", "账户或者密码错误，登陆失败！");
            } else if (exception instanceof DisabledException) {
                map.put("msg", "账户被禁用，登陆失败！");
            } else if (exception instanceof AccountExpiredException) {
                map.put("msg", "账户已过期，登陆失败！");
            } else if (exception instanceof CredentialsExpiredException) {
                map.put("msg", "密码已过期，登陆失败！");
            } else {
                map.put("msg", "登陆失败！");
            }
            System.out.println(exception.getClass().getSimpleName());
            pw.write(new ObjectMapper().writeValueAsString(map));
            pw.flush();
            pw.close();
        }
    }
    ```
4. 修改WebSecurityConfig登录成功或失败走自己的处理器
    ```java
     /**
         * 将自定义的登录成功处理器注入进来
         */
        @Autowired
        private AuthenticationSuccessHandler authenticationSuccessHandler;
    
        /**
         * 将自定义的登录失败处理器注入进来
         */
        @Autowired
        private AuthenticationFailureHandler authenticationFailureHandler;
    
        /**
         * 配置http请求验证等
         *
         * @param http
         * @throws Exception
         */
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // 注释掉他自己的方法 走我们自己的
            // super.configure(http);
            // 给一个异常处理器，走我们自定义的拒绝访问处理器
            http.exceptionHandling().accessDeniedHandler(accessDeniedHandler);
    
            http.formLogin().successHandler(authenticationSuccessHandler).failureHandler(authenticationFailureHandler);
            
            // 给一个表单登陆 就是我们的登录页面,登录成功或者失败后走我们的url
    //        http.formLogin().successForwardUrl("/welcome").failureForwardUrl("/fail");
            // 匹配哪些url，需要哪些权限才可以访问 当然我们也可以使用链式编程的方式
            http.authorizeRequests()
    //                .antMatchers("/query").hasAnyAuthority("sys:query")
    //                .antMatchers("/save").hasAnyAuthority("sys:save")
    //                .antMatchers("/del").hasAnyAuthority("sys:del")
    //                .antMatchers("/update").hasAnyAuthority("sys:update")
    //                .antMatchers("/admin/**").hasRole("ADMIN")
                    .anyRequest().authenticated(); // 其他所有的请求都需要登录才能进行
        }
    ```
## 使用数据库用户
1. webSecurityConfig中注入
    ```java
       @Autowired
        private UserDetailsServiceImpl userDetailsService;
    ```
2. 在config中添加
    ```java
       //基于数据库配置用户信息
       .userDetailsService(userDetailsService)
    ```
3. UserDetailsServiceImpl
    ```java
    /*
        Security的UserDetailsService的实现类对象
            是通过数据库查询必须要实现的一个业务层接口
     */
    @Service
    public class UserDetailsServiceImpl extends ServiceImpl<UserMapper, User> implements UserDetailsService, UserService {
    
        @Autowired
        private UserMapper userMapper;
    
        @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    
            //根据用户名查询用户信息
            //数据库中存储的就是通过BCrypt方式加密的密码
            User user = userMapper.selectOne(
                    new LambdaQueryWrapper<User>()
                            .eq(StringUtils.isNotBlank(username), User::getUsername, username)
            );
    
            if(!ObjectUtils.isEmpty(user)){
    
                //查询用户的权限信息
                /*
                    根据用户id，查询user_role表得到roleIds列表
                    再根据roleIds的列表数据，查询权限信息
                 */
                //List<Map<String,Object>> permissionList = userMapper.selectPermissionByUserId(user.getUserid());
                //List<Permission> permissionList = userMapper.selectPermissionByUserId(user.getUserid());
                List<String> perCodeList = userMapper.selectPercodeByUserId(user.getUserid());
    
                //封装权限信息
                user.setPerCodeList(perCodeList);
    
                //让Security权限框架来校验
                return user;
            }
    
            //用户不存在、用户名密码错误
            return null;
        }
    }
    ```
