package com.szz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szz.domain.User;


/**
 *
 */
public interface UserService extends IService<User> {

}
