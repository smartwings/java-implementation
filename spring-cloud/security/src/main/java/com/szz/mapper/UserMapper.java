package com.szz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.szz.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Entity com.powernode.springcloud.domain.User
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Select({
            "select percode from sys_permission p",
            "left join sys_role_permission rp on p.perid = rp.perid",
            "left join sys_user_role ur on rp.roleid = ur.roleId",
            "where ur.userid = #{userId}"
    })
    List<String> selectPercodeByUserId(Integer userid);
}




