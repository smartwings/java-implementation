package com.szz.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 
 * @TableName sys_user
 */
@TableName(value ="sys_user")
@Data
public class User implements Serializable, UserDetails {
    /**
     * 
     */
    @TableId(value = "userid", type = IdType.AUTO)
    private Integer userid;

    /**
     * 
     */
    @TableField(value = "username")
    private String username;

    /**
     * 
     */
    @TableField(value = "userpwd")
    private String userpwd;

    /**
     * 
     */
    @TableField(value = "sex")
    private String sex;

    /**
     * 
     */
    @TableField(value = "address")
    private String address;

    /**
     * 用户状态字段(1:正常 0:禁用)
     */
    @TableField(value = "status")
    private Integer status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<String> perCodeList;

    //框架调用该方法进行授权操作
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
//        List<GrantedAuthority> auths = new ArrayList<>();
//        //普通方式
//        perCodeList.forEach(
//                perCode ->{
//                    //授权
//                    SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(perCode);
//
//                    auths.add(simpleGrantedAuthority);
//                }
//        );
//        return auths;

        //Lambda方式
        //Stream的api就是用于去高效处理数据
        return perCodeList.stream()
                //通过map方法进行组装新的集合，遍历perCodeList，通过SimpleGrantedAuthority::new创建新集合的对象
                .map(SimpleGrantedAuthority::new)
                //转换成一个新的集合数据
                .collect(Collectors.toList());
                //perCodeList.stream().map(perCode -> new SimpleGrantedAuthority(perCode)).collect(Collectors.toList());
    }

    @Override
    public String getUsername() {
        return String.valueOf(this.userid);
    }

    @Override
    public String getPassword() {
        return this.userpwd;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.status == 1;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.status == 1;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.status == 1;
    }

    @Override
    public boolean isEnabled() {
        return this.status == 1;
    }
}