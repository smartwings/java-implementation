package com.szz.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PermissionsController {
    /**
     * 登录成功后的欢迎页面
     *
     * @return
     */
    @PostMapping("welcome")
    public String welcome() {
        return "我是登录成功后的欢迎页面welcome";
    }

    @GetMapping("save")
    @PreAuthorize("hasAuthority('sys:save')")
    public String save() {
        return "我是save页面";
    }

    @GetMapping("del")
    @PreAuthorize("hasAuthority('sys:del')")
    public String del() {
        return "我是del页面";
    }

    @GetMapping("update")
    @PreAuthorize("hasAuthority('sys:update')")
    public String update() {
        return "我是update页面";
    }

    @GetMapping("query")
    @PreAuthorize("hasAuthority('sys:query')")
    public String query() {
        return "我是query页面";
    }

    @GetMapping("admin/hello")
    public String admin() {
        return "我是只有admin角色才可以访问的";
    }
}
