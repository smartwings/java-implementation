package com.szz.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
@RestController
public class UserController {
    /**
     * 获取当前用户信息，直接在参数中注入Principal对象
     * 此对象是登录后自动写入UsernamePasswordAuthenticationToken类中
     *
     * @param principal
     * @return
     */
    @GetMapping("userInfo")
    public Principal getUserInfo(Principal principal) {
        return principal;
    }

    /**
     * SecurityContextHolder.getContext()获取安全上下文对象
     * 就是那个保存在 ThreadLocal 里面的安全上下文对象
     * 总是不为null(如果不存在，则创建一个authentication属性为null的empty安全上下文对象)
     * 获取当前认证了的 principal(当事人),或者 request token (令牌)
     * 如果没有认证，会是 null,该例子是认证之后的情况
     */
    @GetMapping("userInfo2")
    public Object getUserInfo2() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication;
    }
}
