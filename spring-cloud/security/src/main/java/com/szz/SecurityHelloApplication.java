package com.szz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity // 启用security 在5.X版本之后可以不用加，默认就是开启的
public class SecurityHelloApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityHelloApplication.class, args);
    }

}
