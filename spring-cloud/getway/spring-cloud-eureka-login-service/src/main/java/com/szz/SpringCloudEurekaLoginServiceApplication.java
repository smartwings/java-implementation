package com.szz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SpringCloudEurekaLoginServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudEurekaLoginServiceApplication.class, args);
    }

}
