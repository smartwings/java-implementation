# 远程调用
## 使用(全部在消费者服务)
1.Feign接口
```java
//value需要与服务名一致
@FeignClient(value = "provider-service")
public interface Feign {
    /**
     * 描述: 下单的方法 这里的路径必须和提供者的路径一致
     *
     * @param :
     * @return java.lang.String
     */
    @GetMapping("doOrder")
    String doOrder();

}
```
2.使用feign中方法
```java
@Autowired
private Feign feign;

/**
 * 用户远程调用下单的接口
 *
 * @return
 */
@GetMapping("userDoOrder")
public String userDoOrder() {
    String result = feign.doOrder();
    System.out.println(result);
    return result;
}
```
3.启动类添加注解  
@EnableFeignClients
