package com.szz.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "provider-service")
public interface Feign {
    /**
     * 描述: 下单的方法 这里的路径必须和提供者的路径一致
     *
     * @param :
     * @return java.lang.String
     */
    @GetMapping("doOrder")
    String doOrder();

}
