package com.szz;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.util.ObjectUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JWTTest {

    /**
     * 用户登录成功后，生成一个token
     *
     * @param userId 此时userId不能为null
     * @return
     */
    public static String createToken(Long userId) {
        // 生成的时间
        Date createTime = new Date();
        // 创建过期时间1天
        Calendar nowTime = Calendar.getInstance();
        // 当前时间加上 1天
        nowTime.add(Calendar.DATE, 1);
        // 得到过期时间
        Date expireTime = nowTime.getTime();
        // 设置header信息
        HashMap<String, Object> header = new HashMap<>(4);
        header.put("alg", "HS256");
        header.put("typ", "JWT");
        // 生成token
        String token = JWT.create()
                .withHeader(header) // 设置头部
                .withClaim("userId", userId) // 设置载荷
                .withClaim("username", "cxs")
                .withIssuedAt(createTime) // 创建时间
                .withExpiresAt(expireTime)  // 过期时间
                .sign(Algorithm.HMAC256("cxs-jwt")); // 设置签名秘钥
        return token;
    }

    /**
     * 解密jwt
     *
     * @param token
     * @return
     */
    public static Map<String, Claim> verifyToken(String token) {
        DecodedJWT jwt = null;
        try {
            // 根据签名解密
            JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256("cxs-jwt")).build();
            // 得到jwt对象
            jwt = jwtVerifier.verify(token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 拿到载荷
        Map<String, Claim> claims = jwt.getClaims();
        return claims;
    }

    /**
     * 获取userId
     *
     * @param token
     * @return
     */
    public static Long getUserId(String token) {
        // 先验证验证token拿到map
        Map<String, Claim> claimMap = verifyToken(token);
        Claim userId = claimMap.get("userId");
        if (ObjectUtils.isEmpty(userId)) {
            throw new IllegalArgumentException("参数异常");
        }
        // 拿到具体值
        return userId.asLong();
    }

    /**
     * 测试
     *
     * @param args
     */
    public static void main(String[] args) {
        String token = createToken(2L);
        System.out.println(token);

        Map<String, Claim> stringClaimMap = verifyToken(token);
        System.out.println(stringClaimMap);

        Long userId = getUserId(token);
        System.out.println(userId);
    }

}