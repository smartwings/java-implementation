package com.szz.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class TestController {
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 测试 ribbon 的负载均衡
     *
     * @param serviceId
     * @return
     */
    @RequestMapping("/testRibbonBalance")
    public String testRibbonBalance(String serviceId) {
//直接用服务名称替换 ip:port
        String url = "http://" + serviceId + "/demo";
        System.out.println(url);
        String forObject = restTemplate.getForObject(url,
                String.class);
        System.out.println(forObject);
        return forObject;
    }
//-------------------------无ribbon
//    @Autowired
//    private DiscoveryClient discoveryClient;
//    static Random random = new Random();
//    @RequestMapping("/testBalance")
//    public String testBalance(String serviceId) {
//        //获取服务列表
//        List<ServiceInstance> instances = discoveryClient.getInstances(serviceId);
//        if (ObjectUtils.isEmpty(instances)) {
//            return "服务列表为空";
//        }
//        //如果服务列表不为空，先自己做一个负载均衡
//        ServiceInstance serviceInstance = loadBalance(instances);
//        String host = serviceInstance.getHost();
//        int port = serviceInstance.getPort();
//        String url = "http://" + host + ":" + port + "/demo";
//        System.out.println("本次我调用的是" + url);
//        String forObject = restTemplate.getForObject(url, String.class);
//        System.out.println(forObject);
//        return forObject;
//    }
//    private ServiceInstance loadBalance(List<ServiceInstance> instances) {
////拼接 url 去调用 ip:port 先自己实现不用 ribbon
//        ServiceInstance serviceInstance =
//                instances.get(random.nextInt(instances.size()));
//        return serviceInstance;
//    }
}
