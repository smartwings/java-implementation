package com.szz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
public class SpringCloudEurekaRibbonConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudEurekaRibbonConsumerApplication.class, args);
    }

    //    /**
//     * 用来发请求的
//     *
//     * @return
//     */
//    @Bean
//    public RestTemplate restTemplate() {
//        return new RestTemplate();
//    }
//----------------------ribbon
    @Bean
    @LoadBalanced //ribbon 的负载均衡注解
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
