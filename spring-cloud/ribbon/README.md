# 负载均衡
## 使用
1.启动类添加(仅消费者)
```java
@Bean
@LoadBalanced //ribbon 的负载均衡注解
public RestTemplate restTemplate() {
    return new RestTemplate();
}
```
2.pom.xml
```xml
<!-- 由于netflix-eureka-client中已经集成了ribbon，所以导包的时候只要netflix-eureka-client这个就可以了。-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
</dependency>
```
3.调用
```java
public String testRibbonBalance(String serviceId) {
//直接用服务名称替换 ip:port
    String url = "http://" + serviceId + "/demo";
    System.out.println(url);
//远程类
    String forObject = restTemplate.getForObject(url,
            String.class);
    System.out.println(forObject);
    return forObject;
}

```
## [配置](https://www.jianshu.com/p/25371781ce5c)
### 负载均衡策略
轮询RoundRobinRule，这也是默认的策略；  
随机RandomRule  
响应时间权重ResponseTimeWeightedRule，为每个服务设置权重，响应时间越短，权重越大，下次越有可能被选中；  
最少并发数策略BestAvailableRule  
重试策略RetryRule  
可用性敏感策略AvailabilityFilteringRule  
区域性敏感策略ZoneAvoidanceRule  
总共有七种负载均衡策略可供选择，可以根据自己的业务场景进行选择。  

### 更换负载均衡策略  

#### 方法一、增加一个配置类
```java
@Configuration
public class RibbonBalancedStrategyConfig {
    @Bean
    public IRule ribbonRule(){
        return new RandomRule();
    }

}
```
该配置类对全局Ribbon生效

#### 方法二、使用配置文件
有时候，我们Ribbon可能会负载均衡多个服务源，对于某个服务源我们想更改负载均衡策略，而不是全部，那么就可以在配置文件中进行配置。
```properties
# 指定eureka-client服务源的负载均衡策略
eureka-client.ribbon.NFLoadBalancerRuleClassName=com.netflix.loadbalancer.RandomRule
```
### 超时与重试
同样的，在配置文件中进行配置即可。
```properties
eureka-client.ribbon.ConnectTimeout=1
eureka-client.ribbon.ReadTimeout=1
eureka-client.ribbon.MaxAutoRetries=1
eureka-client.ribbon.MaxAutoRetriesNextServer=1
eureka-client.ribbon.OkToRetryOnAllOperations=true
```
### 饥饿加载
我们的ribbon-client客户端在启动的时候并不会立即加载上下文，需要在请求真正到来时才会创建，
如此会有可能导致第一次调用某个服务源的时候出现很慢的情况，
我们可以通过如下配置项的方式来指定具体的服务名称开启饥饿加载，即在启动的时候就加载所有服务应用上下文。
```properties
ribbon.eager-load.enabled=true
ribbon.eager-load.clients=eureka-client,eureka-client2
```


