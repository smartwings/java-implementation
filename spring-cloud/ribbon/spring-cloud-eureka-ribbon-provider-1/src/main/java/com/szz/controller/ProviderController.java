package com.szz.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProviderController {
    @RequestMapping("/demo")
    public String demo() {
        String str = "我是provider1";
        return str;
    }
}
