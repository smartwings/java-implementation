# Java实现
## 微服务
### Eureka使用
spring-cloud->eureka
1. spring-cloud-eureka-server(Eureka服务,注册中心)
2. spring-cloud-eureka-client-provider(提供者)
3. spring-cloud-eureka-client-consumer(消费者)
4. spring-cloud-eureka-common(公共类)
### Nacos使用
spring-cloud->nacos
1. spring-cloud-nacos-client-provider(提供者)
2. spring-cloud-nacos-client-consumer(消费者)
3. spring-cloud-nacos-common(公共类)
### Ribbon使用(spring-cloud->ribbon)
spring-cloud->ribbon
1. spring-cloud-eureka-server(Eureka服务,注册中心)
2. spring-cloud-eureka-ribbon-consumer(消费者,配置ribbon)
3. spring-cloud-eureka-ribbon-provider-1(提供者1)
4. spring-cloud-eureka-ribbon-provider-2(提供者2)
### OpenFeign使用
spring-cloud->openfeign
1. spring-cloud-eureka-server(Eureka服务,注册中心)
2. spring-cloud-open-feign-consumer
3. spring-cloud-open-feign-provider
### Hystrix使用
spring-cloud->hystrix
1. spring-cloud-eureka-server(Eureka服务,注册中心)
2. spring-cloud-open-feign-hystrix-consumer
3. spring-cloud-open-feign-hystrix-provider
### Gateway 使用
spring-cloud->gateway
1. spring
## 增删改查的实现,由javase到springboot(待修改为使用demo.sql,现为vhr.sql)
全部使用maven,数据库:mysql5.7
若数据库为8的需要修改驱动为: com.mysql.cj.jdbc.Driver
需要修改pom.xml中mysql-connector-java的版本:8.xx.xx
1. my-vhr-javase 
   - JavaSE
   - JdbcUtils类
2. my-vhr-javaweb-html
   - HTML
   - Servlet
   - JdbcUtils类
3. my-vhr-javaweb-jsp
   - Jsp
   - Servlet
   - JdbcUtils类
4. my-vhr-javaweb-ajax
   - HTML
   - Ajax
   - Servlet
   - JdbcUtils类
5. my-vhr-mybatis
   - Jsp
   - Servlet
   - Mybatis
6. my-vhr-spring-mybatis
   - Jsp
   - Servlet
   - Spring
   - Mybatis
7. my-vhr-ssm
   - JSP
   - SpringMVC
   - Spring
   - Mybatis
8. my-vhr-springboot-vue
   - HTML
   - Vue
   - Springboot
   - Mybatis