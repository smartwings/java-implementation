package com.szz.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitMQConfig {
    @Bean
    public Queue queue(){
        //默认创建的是持久化、非自动删除的消息队列、非排外(当前消息队列只能有一个消费者来消费)
        return new Queue("baseQueue");
    }

    //---------------------
    //声明消息队列、交换机、绑定关系(指定路由键)
    @Bean
    public Queue directQueue(){
        //默认创建的是持久化、非自动删除的消息队列、非排外(当前消息队列只能有一个消费者来消费)
        return new Queue("directQueue");
    }

    @Bean
    public Exchange directExchange(){
        //默认创建的是非自动删除持久化的交换机
        return new DirectExchange("directExchange");
    }

    @Bean
    public Binding directBinding(){
        return BindingBuilder.bind(directQueue()).to(directExchange()).with("directRoutingKey").noargs();
    }

    //--------------------
    @Bean
    public Queue topicQueue1(){
        return new Queue("topicQueue1");
    }

    @Bean
    public Queue topicQueue2(){
        return new Queue("topicQueue2");
    }

    @Bean
    public Queue topicQueue3(){
        return new Queue("topicQueue3");
    }

    @Bean
    public Exchange topicExchange(){
        //默认创建的是非自动删除持久化的交换机
        return new TopicExchange("topicExchange");
    }

    //使用统配规则来声明绑定关系
    @Bean
    public Binding topicBinding1(){
        return BindingBuilder.bind(topicQueue1()).to(topicExchange()).with("aa").noargs();
    }

    @Bean
    public Binding topicBinding2(){
        return BindingBuilder.bind(topicQueue2()).to(topicExchange()).with("aa.*").noargs();
    }

    @Bean
    public Binding topicBinding3(){
        return BindingBuilder.bind(topicQueue3()).to(topicExchange()).with("aa.#").noargs();
    }

    //--------------声明死信队列
    @Bean
    public Queue test(){

        Map<String, Object> arguments = new HashMap<>();
        //创建私信交换机和死信路由键
        arguments.put("x-message-ttl",30000);
        arguments.put("x-dead-letter-exchange","deadLetterExchange");
        arguments.put("x-dead-letter-routing-key","deadLetterRoutingKey");

        return new Queue("test",true,false,false,arguments);
    }

    @Bean
    public Queue deadLetterQueue(){
        return new Queue("deadLetterQueue");
    }

    @Bean
    public Exchange deadLetterExchange(){
        return new DirectExchange("deadLetterExchange");
    }

    @Bean
    public Binding deadLetterBindings(){
        return BindingBuilder.bind(deadLetterQueue()).to(deadLetterExchange()).with("deadLetterRoutingKey").noargs();
    }
}
