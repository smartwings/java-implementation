package com.szz.send;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SendMessage {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /*
        RabbitMQ的消息发送
            1. 安装otp_win64_17.3.exe文件，erlang语言环境，并配置环境变量
                和安装Java配置Java环境变量类似
                erl -v 按ctrl + c 退出
            2. 安装rabbitmq-server-3.10.7.exe文件，RabbitMQServer
                a. 安装管理控制台的插件
                    进入rabbitmq的sbin目录执行命令：rabbitmq-plugins.bat enable rabbitmq_management，安装管理控制台组件
                b. 启动RabbitMQ命令：rabbitmqctl.bat start_app
            3. 浏览器输入管理控制台地址，访问
                http://localhost:15672
                用户名和密码默认为guest
            4. pom.xml中引入起步依赖
            5. application.yml配置文件中进行添加配置信息
            6. 通过配置类来声明，交换机、消息队列、绑定关系
            7. 通过api来发送消息
                注入模板对象，AmqpTemplate(父)或者RabbitTemplate(子)
                调用api发送消息
     */

    //使用默认的Direct类型交换机来发送消息，没有指定routingKey
    public void sendMsg01() throws JsonProcessingException {
        //参数1，routingKey，如果想使用**默认的交换机**来发送消息，这里的routingKey指的就是消息队列名称
        //参数2，发送的消息json
        Map<String,Object> user = new HashMap<>();
        user.put("id",111);
        user.put("username","张三");
        user.put("age",22);

        rabbitTemplate.convertAndSend(
                "baseQueue",new ObjectMapper().writeValueAsString(user)
        );
    }


    public void sendMsg02() throws JsonProcessingException {
        //参数1，交换机名称
        //参数2，routingKey
        //参数3，消息
        Map<String,Object> user = new HashMap<>();
        user.put("id",222);
        user.put("username","李四");
        user.put("age",23);

        rabbitTemplate.convertAndSend(
                "directExchange","directRoutingKey",new ObjectMapper().writeValueAsString(user)
        );
    }


    public void sendMsg03() throws JsonProcessingException {
        //参数1，交换机名称
        //参数2，routingKey
        //参数3，消息
        Map<String,Object> user = new HashMap<>();
        user.put("id",333);
        user.put("username","王五");
        user.put("age",24);

        rabbitTemplate.convertAndSend(
                //这里发送的消息是到FanoutExchange交换机中，没有路由键的，指定空字符串即可
                "fanoutExchange","",new ObjectMapper().writeValueAsString(user)
        );
    }

    public void sendMsg04() throws JsonProcessingException {
        //参数1，交换机名称
        //参数2，routingKey
        //参数3，消息
        Map<String,Object> user1 = new HashMap<>();
        user1.put("id",444);
        user1.put("username","赵六");
        user1.put("age",25);

        Map<String,Object> user2 = new HashMap<>();
        user2.put("id",555);
        user2.put("username","田七");
        user2.put("age",26);

        Map<String,Object> user3 = new HashMap<>();
        user3.put("id",666);
        user3.put("username","吴八");
        user3.put("age",27);

        rabbitTemplate.convertAndSend(
                //这里发送的消息是到FanoutExchange交换机中，没有路由键的，指定空字符串即可
                "topicExchange","aa",new ObjectMapper().writeValueAsString(user1)
        );
        rabbitTemplate.convertAndSend(
                //这里发送的消息是到FanoutExchange交换机中，没有路由键的，指定空字符串即可
                "topicExchange","aa.bb",new ObjectMapper().writeValueAsString(user2)
        );
        rabbitTemplate.convertAndSend(
                //这里发送的消息是到FanoutExchange交换机中，没有路由键的，指定空字符串即可
                "topicExchange","aa.bb.cc",new ObjectMapper().writeValueAsString(user3)
        );
    }


    public void sendMsg05() throws JsonProcessingException {
        Map<String,Object> user = new HashMap<>();
        user.put("id",111);
        user.put("goodsName","IPhone 1000 Pro Max苍蝇绿");
        user.put("price",1);
        user.put("stock",1);

        rabbitTemplate.convertAndSend(
                "test",new ObjectMapper().writeValueAsString(user)
        );
    }
}
