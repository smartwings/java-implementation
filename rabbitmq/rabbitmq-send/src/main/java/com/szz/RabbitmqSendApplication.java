package com.szz;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.szz.send.SendMessage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class RabbitmqSendApplication {

    public static void main(String[] args) throws JsonProcessingException {
        ConfigurableApplicationContext run = SpringApplication.run(RabbitmqSendApplication.class, args);
        SendMessage bean = run.getBean(SendMessage.class);

        //发送基本消息到消息队列中
        //bean.sendMsg01();

        //发送消息到Direct类型交换机中
        //bean.sendMsg02();

        //发送消息到Fanout类型交换机中
        //bean.sendMsg03();

        //发送消息到Topic类型交换机中
        //bean.sendMsg04();

        //发送消息到死信交换机中
        bean.sendMsg05();
    }

}
