package com.szz.listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQListener {

    @RabbitListener(queues = {"baseQueue"})
    //@RabbitListener(queues = {"baseQueue","directQueue"})
    public void receiveBaseQueueMsg(Message message){
        String json = new String(message.getBody());

        System.out.println("接收到消息："+json);
    }

    @RabbitListener(queues = {"directQueue"})
    public void receiveDirectQueueMsg(Message message){
        String json = new String(message.getBody());

        System.out.println("接收到消息："+json);
    }


    //通过消费者声明监听关系，也可以声明消息队列、交换机和绑定关系
    @RabbitListener(
            //声明绑定关系
            bindings={
                    @QueueBinding(
                            //消息队列的声明，如果没有指定消息队列名称，则使用的是随机名称的消息队列
                            value = @Queue,
                            //交换机的声明
                            exchange = @Exchange(name = "fanoutExchange",type = "fanout")
                    )
            }
    )
    public void receiveFanoutQueueMsg(Message message){
        String json = new String(message.getBody());

        System.out.println("接收到消息："+json);
    }


    @RabbitListener(queues = {"topicQueue1","topicQueue2","topicQueue3"})
    public void receiveTopicQueueMsg(Message message){
        String json = new String(message.getBody());
        System.out.println("接收到消息："+message.getMessageProperties().getConsumerQueue() + " -> "+json);
    }



    @RabbitListener(queues = {"deadLetterQueue"})
    public void receiveDeadLetterQueueMsg(Message message){
        String json = new String(message.getBody());

        System.out.println("接收到消息：进行消息补偿 -> "+json);
    }

}