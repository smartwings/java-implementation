package org.example;

import org.example.entity.Employee;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class AppTest {
    @Test
    public void setContext() {
        //根据 spring的配置文件 创建 应用容器
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml");
        System.out.println("马上获取bean");
        //从容器中获取对象
        Employee employee = (Employee) context.getBean("Employee");
        System.out.println(employee);
    }


}
