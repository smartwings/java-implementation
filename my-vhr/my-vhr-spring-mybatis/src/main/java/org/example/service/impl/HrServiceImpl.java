package org.example.service.impl;


import org.example.mapper.HrMapper;
import org.example.service.HrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(rollbackFor = Exception.class)
public class HrServiceImpl implements HrService {
    @Autowired
    private HrMapper mapper;


    @Override
    public String login(String username, String password) {
        if (null == username || null == password) {
            return "用户名或密码为空，请重新输入";
        } else {
            if (null != mapper.login(username, password)) {
                return "登录成功";
            } else {
                return "登录失败";
            }
        }
    }
}
