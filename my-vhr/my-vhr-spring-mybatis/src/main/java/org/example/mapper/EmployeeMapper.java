package org.example.mapper;

import org.example.entity.Employee;

import java.util.List;


public interface EmployeeMapper {


    int addEmployee(Employee employee);


    int updateEmployee(Employee employee);


    int deleteEmployee(int id);


    Employee queryEmployee(int id);


    int count();


    List<Employee> selectAll();
}
