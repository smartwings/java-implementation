package org.example.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;


@WebFilter
public class EncodingFilter implements Filter {


    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        //设置字符编码
        if (null == req.getCharacterEncoding()) {
            req.setCharacterEncoding("UTF-8");
            resp.setContentType("text/html;charset=UTF-8");
        }
        chain.doFilter(req, resp);
    }


    @Override
    public void destroy() {

    }


    @Override
    public void init(FilterConfig config) throws ServletException {

    }

}
