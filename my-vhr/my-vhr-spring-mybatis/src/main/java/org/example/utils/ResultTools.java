package org.example.utils;

import lombok.Data;


@Data
public class ResultTools {


    private Boolean flag = true;


    private Integer code = 200;


    private String msg;


    private Object data;


    public ResultTools() {
    }


    public ResultTools(Boolean flag, Integer code, String msg, Object data) {
        this.flag = flag;
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
