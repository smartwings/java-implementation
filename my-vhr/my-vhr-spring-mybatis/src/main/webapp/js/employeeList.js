window.onload = function () {
    const queryAll = document.getElementById("queryAll");
    const addEmployee = document.getElementById("addEmployee");
    const updateEmployee = document.getElementById("updateEmployee");
    const exit = document.getElementById("exit");

    queryAll.onclick = function () {
        window.open("/springMybatis/employeeList?pageNum=1&pageSize=10", '_self')
    };
    addEmployee.onclick = function () {
        window.open("/springMybatis/addEmployee.jsp", '_self')
    };
    updateEmployee.onclick = function () {
        window.open("/springMybatis/updateEmployee.jsp", '_self')
    };
    exit.onclick = function () {
        window.open("/springMybatis/login.jsp", '_self')
    };

}