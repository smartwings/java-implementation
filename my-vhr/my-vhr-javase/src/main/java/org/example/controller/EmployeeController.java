package org.example.controller;

import org.example.entity.Employee;
import org.example.service.EmployeeService;
import org.example.service.impl.EmployeeServiceImpl;
import org.example.utils.CheckTools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;


public class EmployeeController {


    private static final int ROW_PAGE = 20;


    private static final String YES_SMALL = "y";


    private static final String YES_BIG = "Y";


    private static final String YES_WEBLOCK = "已婚";


    private static final String NO_WEBLOCK = "未婚";


    private EmployeeService employeeService = new EmployeeServiceImpl();


    public void queryAll() {
        // 存储查询出的数据
        List<Employee> employees = null;
        // 查询数据库总共条数
        int num = employeeService.count();
        // 默认总数据大于20条,使用户选择（分页雏形）
        if (num > ROW_PAGE) {
            System.out.println("员工信息一共" + num + "条大于20人,请选择查看第几条到第几条");
            System.out.println("开始条数");
            int num1 = CheckTools.isDigit();
            System.out.println("结束条数(不包含结束条数)");
            int num2 = CheckTools.isDigit();
            if (num2 < num1) {
                System.out.println("开始条数应小于结束条数");
            } else {
                employees = employeeService.queryPart(num1, num2);
            }
        } else {
            employees = employeeService.queryAll();
        }
        System.out.println("id\t\t\t姓名\t\t\t性别\t\t\t生日\t\t\t结婚状态");
        for (Employee e : employees) {
            System.out.println(e.toString());
        }
    }


    public void addEmployee() {
        Employee employee = new Employee();
        Employee employee1 = inEmployee(employee);
        if (null == employee1.getId()) {
            System.out.println("添加失败");
        } else {
            if (employeeService.addEmployee(employee1)) {
                System.out.println("添加成功");
            } else {
                System.out.println("添加失败");
            }
        }

    }


    public void updateEmployee() {
        System.out.println("请输入修改人ID");
        int id = CheckTools.isDigit();
        Employee employees = employeeService.queryEmployee(id);
        if (null == employees) {
            System.out.println("查无此人");
        } else {
            System.out.println("是否修改该条信息y/n");
            Scanner scanner = new Scanner(System.in);
            String s = scanner.next();
            if (YES_SMALL.equals(s) || YES_BIG.equals(s)) {
                Employee employee = new Employee();
                employee.setId(id);
                System.out.println("请输入修改信息:");
                Employee employee1 = inEmployee(employee);
                if (null == employee1.getId()) {
                    System.out.println("添加失败");
                } else {
                    if (employeeService.updateEmployee(employee1)) {
                        System.out.println("添加成功");
                    } else {
                        System.out.println("添加失败");
                    }
                }
            }
        }
    }


    public void deleteEmployee() {
        System.out.println("请输入删除人ID");
        int id = CheckTools.isDigit();
        if (employeeService.deleteEmployee(id)) {
            System.out.println("删除成功");
        } else {
            System.out.println("删除失败");
        }
    }


    private Employee inEmployee(Employee employee) {
        System.out.println("姓名");
        Scanner name = new Scanner(System.in);
        employee.setName(name.next());

        System.out.println("性别");
        Scanner gender = new Scanner(System.in);
        employee.setGender(gender.next());

        System.out.println("生日(格式YYYY/MM/DD)");
        Scanner birthday = new Scanner(System.in);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String date = birthday.next();
        Date birthdayDate = null;
        try {
            birthdayDate = simpleDateFormat.parse(date);
            employee.setBirthday(birthdayDate);

            System.out.println("结婚状态(已婚/未婚)");
            Scanner weblock = new Scanner(System.in);
            String s1 = weblock.next();
            if (YES_WEBLOCK.equals(s1) || NO_WEBLOCK.equals(s1)) {
                employee.setWedlock(s1);
            } else {
                System.out.println("输入错误");
                return new Employee();
            }
        } catch (ParseException e) {
            System.out.println("格式错误");
            employee = new Employee();
        }
        return employee;
    }
}
