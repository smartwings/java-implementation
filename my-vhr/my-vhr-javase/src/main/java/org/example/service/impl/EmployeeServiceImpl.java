package org.example.service.impl;

import org.example.dao.EmployeeDao;
import org.example.dao.impl.EmployeeDaoImpl;
import org.example.entity.Employee;
import org.example.service.EmployeeService;

import java.util.List;


public class EmployeeServiceImpl implements EmployeeService {
    EmployeeDao employeeDao = new EmployeeDaoImpl();


    @Override
    public List<Employee> queryAll() {
        return employeeDao.queryAll();
    }


    @Override
    public Boolean addEmployee(Employee employee) {
        if (employeeDao.addEmployee(employee) > 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Boolean updateEmployee(Employee employee) {
        if (employeeDao.updateEmployee(employee) > 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Boolean deleteEmployee(Integer id) {
        if (employeeDao.deleteEmployee(id) > 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Employee queryEmployee(Integer id) {
        return employeeDao.queryEmployee(id);
    }


    @Override
    public int count() {
        return employeeDao.count();
    }


    @Override
    public List<Employee> queryPart(int num1, int num2) {
        if (num1 < 1) {
            return employeeDao.queryPart(num1, num2 - num1);
        } else {
            return employeeDao.queryPart(num1 - 1, num2 - num1);
        }

    }

}
