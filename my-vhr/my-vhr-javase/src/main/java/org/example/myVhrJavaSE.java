package org.example;

import org.example.controller.EmployeeController;
import org.example.utils.CheckTools;


public class myVhrJavaSE {


    public static void main(String[] args) {
        EmployeeController employeeController = new EmployeeController();
        while (true) {
            System.out.println("=====欢迎使用员工管理系统=====");
            System.out.println("1.查询所有员工");
            System.out.println("2.添加员工");
            System.out.println("3.删除员工");
            System.out.println("4.修改员工");
            System.out.println("5.退出");
            int i = CheckTools.isDigit();
            switch (i) {
                case 1:
                    employeeController.queryAll();
                    break;
                case 2:
                    employeeController.addEmployee();
                    break;
                case 3:
                    employeeController.deleteEmployee();
                    break;
                case 4:
                    employeeController.updateEmployee();
                    break;
                case 5:
                    System.exit(8);
                    break;
                default:
                    System.out.println("输入错误！！！");
                    break;

            }
        }
    }


}
