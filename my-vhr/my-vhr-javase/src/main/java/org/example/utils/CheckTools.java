package org.example.utils;

import java.util.Scanner;


public class CheckTools {
    private static final int USER_COUNT = 2;


    public static int isDigit() {
        Scanner scanner = new Scanner(System.in);
        int num = 0;
        try {
            return scanner.nextInt();
        } catch (ClassCastException e) {
            num++;
            System.out.println("请输入数字");
            isDigit();
        }
        return -1;
    }
}
