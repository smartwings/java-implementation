package org.example.servlet;

import org.example.entity.Employee;
import org.example.service.EmployeeService;
import org.example.service.HrService;
import org.example.service.impl.EmployeeServiceImpl;
import org.example.service.impl.HrServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;


@WebServlet(urlPatterns = {"/login", "/addEmployee", "/updateEmployee", "/deleteEmployee", "/employeeList"})
public class MyHttpServlet extends HttpServlet {
    private final static String REMEMBER_PASSWORD = "isLogin";
    private final static String LOGINSUCCESS = "登录成功";
    private final static String LOGIN = "/login";
    private final static String ADD = "/addEmployee";
    private final static String UPDATE = "/updateEmployee";
    private final static String DELETE = "/deleteEmployee";
    private final static String QUERYALL = "/employeeList";
    private final static String QUERY_EMPLOYEE = "/queryEmployee";
    private final static String URL = "<a href='http://localhost:8080/java_web/welcome.html'>返回欢迎页面</a><br/>";
    private HrService hrService = new HrServiceImpl();
    private EmployeeService employeeService = new EmployeeServiceImpl();


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        switch (action) {
            case LOGIN:
                login(request, response);
                break;
            case ADD:
                add(request, response);
                break;
            case UPDATE:
                update(request, response);
                break;
            case DELETE:
                delete(request, response);
                break;
            case QUERYALL:
                queryAll(response);
                break;
            case QUERY_EMPLOYEE:
                String idStr = request.getParameter("id");
                if (idStr != "") {
                    int id = Integer.valueOf(idStr);
                    employeeService.queryEmployee(id);
                }
                break;
            default:
                break;
        }

    }


    public void queryAll(HttpServletResponse response) throws IOException {
        EmployeeService employeeService = new EmployeeServiceImpl();
        List<Employee> employees = employeeService.queryAll();
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.write("<!DOCTYPE html>");
        out.write("<html lang=\"en\">");
        out.write("<head>");
        out.write(" <meta charset=\"UTF-8\">");
        out.write("<title>");
        out.write("员工列表");
        out.write("</title>");
        out.write("</head>");
        out.write("<body>");
        out.write(URL);
        out.write("<table border=\"1\"><tr><th>工号</th><th>名字</th><th>性别</th><th>生日</th><th>结婚状态</th></tr>");
        for (Employee employee : employees) {
            out.write("<tr><td>" + employee.getId());
            out.write("</td><td>" + employee.getName());
            out.write("</td><td>" + employee.getGender());
            out.write("</td><td>" + employee.getBirthday());
            out.write("</td><td>" + employee.getWedlock() + "</td></tr>");
        }
        out.write("</table>");
        out.write("</body>");
        out.write("</html>");
        out.flush();
        out.close();
    }


    public void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String msg = hrService.login(username, password);
        if (LOGINSUCCESS.equals(msg)) {
            if (null != request.getParameter(REMEMBER_PASSWORD)) {
                Cookie username1 = new Cookie("username", username);
                response.addCookie(username1);
            }
            HttpSession session = request.getSession();
            session.setAttribute("username", username);
            request.getRequestDispatcher("welcome.html").forward(request, response);
        } else {
            request.getRequestDispatcher("error.html").forward(request, response);
        }
    }


    private void delete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Integer id = Integer.valueOf(request.getParameter("id"));
        PrintWriter out = response.getWriter();
        if (null != employeeService.queryEmployee(id)) {
            if (employeeService.deleteEmployee(id)) {
                out.print(URL + "<p>删除成功</p>");
                request.getRequestDispatcher("welcome.html").forward(request, response);
            } else {
                out.print(URL + "<p>删除失败<p>");
            }
        } else {
            out.print(URL + "<p>未查到该用户<p>");
        }

    }


    private void update(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Integer id = Integer.valueOf(request.getParameter("id"));
        PrintWriter out = response.getWriter();
        if (null != employeeService.queryEmployee(id)) {
            Employee employee = addAndUpdate(request);
            employee.setId(id);
            if (employeeService.updateEmployee(employee)) {
                out.print(URL + "<p>修改成功</p>");
                request.getRequestDispatcher("welcome.html").forward(request, response);
            } else {
                out.print(URL + "<p>修改失败</p>");
            }
        } else {
            out.print(URL + "<p>未查到该用户</p>");
        }

    }


    private void add(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Employee employee = addAndUpdate(request);
        PrintWriter out = response.getWriter();
        if (employeeService.addEmployee(employee)) {
            out.print(URL + "<p>添加成功</p>");
            request.getRequestDispatcher("welcome.html").forward(request, response);
        } else {
            out.print(URL + "<p>添加失败</p>");
        }
    }


    public Employee addAndUpdate(HttpServletRequest request) {
        String name = request.getParameter("name");
        String gender = request.getParameter("gender");
        String birthday = request.getParameter("birthday");
        String wedlock = request.getParameter("wedlock");

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime format = LocalDate.parse(birthday, dateTimeFormatter).atStartOfDay();
        Date date = Date.from(format.atZone(ZoneId.systemDefault()).toInstant());
        Employee employee = new Employee(name, gender, date, wedlock);
        return employee;
    }

}
