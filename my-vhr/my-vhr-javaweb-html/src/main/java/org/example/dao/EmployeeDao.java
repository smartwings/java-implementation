package org.example.dao;

import org.example.entity.Employee;

import java.util.List;


public interface EmployeeDao {


    List<Employee> queryAll();


    int addEmployee(Employee employee);


    int updateEmployee(Employee employee);


    int deleteEmployee(int id);


    Employee queryEmployee(int id);


    int count();


    List<Employee> queryPart(int num1, int mum2);

}
