package com.example.mapper;

import com.example.entity.Hr;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface HrMapper {


    Hr login(@Param("username") String username, @Param("password") String password);
}
