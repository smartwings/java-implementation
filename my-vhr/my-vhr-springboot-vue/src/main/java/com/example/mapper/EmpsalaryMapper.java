package com.example.mapper;

import com.example.entity.Empsalary;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface EmpsalaryMapper {


    int deleteByPrimaryKey(Integer id);


    int insert(Empsalary record);


    int insertSelective(Empsalary record);


    Empsalary selectByPrimaryKey(Integer id);


    int updateByPrimaryKeySelective(Empsalary record);


    int updateByPrimaryKey(Empsalary record);


    int deleteByForeignKey(int id);
}