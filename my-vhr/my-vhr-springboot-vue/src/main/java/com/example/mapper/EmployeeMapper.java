package com.example.mapper;

import com.example.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface EmployeeMapper {


    int addEmployee(Employee employee);


    int updateEmployee(Employee employee);


    int deleteEmployee(int id);


    Employee queryEmployee(int id);


    int count();


    List<Employee> selectAll();
}
