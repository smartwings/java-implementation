package com.example.controller;

import com.example.entity.Employee;
import com.example.service.EmployeeService;
import com.example.utils.ResultTools;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@CrossOrigin
public class EmployeeController {
    ResultTools result;
    @Resource
    private EmployeeService employeeService;

    @RequestMapping("employeeList")
    @ResponseBody
    public PageInfo employeeView(@RequestParam int pageNum, @RequestParam int pageSize) {
        System.out.println(pageNum);
        System.out.println(pageSize);

        PageHelper.startPage(pageNum, pageSize);
        List<Employee> employees = employeeService.queryAll();

        PageInfo pageInfo = new PageInfo(employees, 10);
        System.out.println(pageInfo);

        return pageInfo;
    }


    @RequestMapping("addEmployee")
    public ResultTools addEmployee(Employee employee) {
        Boolean isSuccess = employeeService.addEmployee(employee);
        if (isSuccess) {
            result = new ResultTools();
        } else {
            result = new ResultTools(false, 500, "失败");
        }
        return result;
    }


    @RequestMapping("updateEmployee")
    @ResponseBody
    public ResultTools updateEmployee(@RequestBody Employee employee) {
        System.out.println(employee);
        Boolean isSuccess = employeeService.updateEmployee(employee);
        if (isSuccess) {
            result = new ResultTools();
        } else {
            result = new ResultTools(false, 500, "失败");
        }
        return result;
    }


    @RequestMapping("deleteEmployee")
    public ResultTools deleteEmployee(int id) {
        Boolean isSuccess = employeeService.deleteEmployee(id);
        if (isSuccess) {
            result = new ResultTools();
        } else {
            result = new ResultTools(false, 500, "失败");
        }
        return result;
    }

    @RequestMapping("getEmployeeById")
    public Employee getEmployeeById(int id) {
        Employee employee = employeeService.queryEmployee(id);
        result = new ResultTools("成功", employee);
        return employee;
    }
}
