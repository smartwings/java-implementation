package com.example.controller;


import com.example.entity.Hr;
import com.example.service.HrService;
import com.example.utils.ResultTools;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@CrossOrigin
public class HrController {
    @Resource
    private HrService hrService;

    @RequestMapping("login")
    @ResponseBody
    public ResultTools login(@RequestBody Hr hr) {
        System.out.println(hr.getUsername());
        if (null == hr.getUsername() || null == hr.getPassword()) {
            return new ResultTools(false, 500, "用户名或密码为空,请重新输入");
        } else {
            if (hrService.login(hr.getUsername(), hr.getPassword())) {
                return new ResultTools();
            } else {
                return new ResultTools(false, 200, "未查询到该用户");
            }
        }
    }
}
