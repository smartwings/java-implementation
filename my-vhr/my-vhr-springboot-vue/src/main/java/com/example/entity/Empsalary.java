package com.example.entity;

import lombok.Data;

import java.io.Serializable;


@Data
public class Empsalary implements Serializable {


    private static final long serialVersionUID = 1L;


    private Integer id;


    private Integer eid;


    private Integer sid;
}