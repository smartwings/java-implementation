package com.example.service;


import com.example.entity.Employee;

import java.util.List;


public interface EmployeeService {


    List<Employee> queryAll();


    Boolean addEmployee(Employee employee);


    Boolean updateEmployee(Employee employee);


    Boolean deleteEmployee(Integer id);


    Employee queryEmployee(Integer id);


    int count();

}
