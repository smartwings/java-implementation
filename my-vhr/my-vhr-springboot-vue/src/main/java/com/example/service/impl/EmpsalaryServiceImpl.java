package com.example.service.impl;


import com.example.entity.Empsalary;
import com.example.mapper.EmpsalaryMapper;
import com.example.service.EmpsalaryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class EmpsalaryServiceImpl implements EmpsalaryService {


    @Resource
    private EmpsalaryMapper empsalaryMapper;


    public int deleteByPrimaryKey(Integer id) {
        return empsalaryMapper.deleteByPrimaryKey(id);
    }


    public Boolean deleteByForeignKey(int id) {
        int num = empsalaryMapper.deleteByForeignKey(id);
        if (num == 0) {
            return false;
        } else {
            return true;
        }
    }


    public int insert(Empsalary record) {
        return empsalaryMapper.insert(record);
    }


    public int insertSelective(Empsalary record) {
        return empsalaryMapper.insertSelective(record);
    }


    public Empsalary selectByPrimaryKey(Integer id) {
        return empsalaryMapper.selectByPrimaryKey(id);
    }


    public int updateByPrimaryKeySelective(Empsalary record) {
        return empsalaryMapper.updateByPrimaryKeySelective(record);
    }


    public int updateByPrimaryKey(Empsalary record) {
        return empsalaryMapper.updateByPrimaryKey(record);
    }

}
