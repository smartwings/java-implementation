package com.example.service;


import com.example.entity.Empsalary;


public interface EmpsalaryService {


    int deleteByPrimaryKey(Integer id);


    Boolean deleteByForeignKey(int id);


    int insert(Empsalary record);


    int insertSelective(Empsalary record);


    Empsalary selectByPrimaryKey(Integer id);


    int updateByPrimaryKeySelective(Empsalary record);


    int updateByPrimaryKey(Empsalary record);
}
