package com.example.service.impl;


import com.example.mapper.HrMapper;
import com.example.service.HrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(rollbackFor = Exception.class)
public class HrServiceImpl implements HrService {
    @Autowired
    private HrMapper mapper;


    @Override
    public Boolean login(String username, String password) {
        if (null != mapper.login(username, password)) {
            return true;
        } else {
            return false;
        }

    }
}
