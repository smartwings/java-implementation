package com.example.utils;

import lombok.Data;


@Data
public class ResultTools {


    private Boolean flag = true;


    private Integer code = 200;


    private String msg = "成功";


    private Object data;

    public ResultTools() {

    }

    public ResultTools(String msg) {
        this.msg = msg;
    }

    public ResultTools(String msg, Object data) {
        this.msg = msg;
        this.data = data;
    }

    public ResultTools(Boolean flag, Integer code, String msg) {
        this.flag = flag;
        this.code = code;
        this.msg = msg;
    }

    public ResultTools(Boolean flag, Integer code, String msg, Object data) {
        this.flag = flag;
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
