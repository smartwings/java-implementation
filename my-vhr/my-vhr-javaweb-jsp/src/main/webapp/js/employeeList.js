window.onload = function () {
    const queryAll = document.getElementById("queryAll");
    const addEmployee = document.getElementById("addEmployee");
    const updateEmployee = document.getElementById("updateEmployee");
    const deleteEmployee = document.getElementById("deleteEmployee");
    const exit = document.getElementById("exit");

    queryAll.onclick = function () {
        window.open("/java_web/employeeList?pageIndex=1&pageSize=10", '_self')
    };
    addEmployee.onclick = function () {
        window.open("/java_web/addEmployee.jsp", '_self')
    };
    updateEmployee.onclick = function () {
        window.open("/java_web/updateEmployee.jsp", '_self')
    };
    deleteEmployee.onclick = function () {
        window.open("/java_web/deleteEmployee.jsp", '_self')
    };
    exit.onclick = function () {
        window.open("/java_web/login.jsp", '_self')
    };

}