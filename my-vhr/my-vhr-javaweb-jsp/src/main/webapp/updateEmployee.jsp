<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script>
        function queryEmployee() {
            const index = document.getElementById("id").value;
            if (index !== null) {
                window.open("/java_web/queryEmployee?id=" + index, '_self')
            }
        }
    </script>
</head>
<body>
<form action="/java_web/updateEmployee" method="get">
    工号：<input type="text" name="id" id="id" onblur="queryEmployee()" value="${result.data.id}"><br>
    姓名：<input type="text" name="name" id="name" value="${result.data.name}"><br>
    性别：<input type="text" name="gender" id="gender" value="${result.data.gender}"><br>
    生日：<input type="date" name="birthday" id="birthday" value="${result.data.birthday}"><br>
    婚姻状态：<input type="text" name="wedlock" id="wedlock" value="${result.data.wedlock}"><br>
    <input type="submit">
</form>
</body>
</html>