<%--
  Created by IntelliJ IDEA.
  User: 34186
  Date: 2022/8/26
  Time: 19:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>员工列表</title>
</head>
<body>
<a href="welcome.jsp">返回欢迎页面</a><br/>
<table border="1">
    <tr>
        <th>工号</th>
        <th>名字</th>
        <th>性别</th>
        <th>生日</th>
        <th>结婚状态</th>
    </tr>
    <c:forEach items="${pageUtils.data}" var="e">
        <tr>
            <td>${e.id}</td>
            <td>${e.name}</td>
            <td>${e.gender}</td>
            <td>${e.birthday}</td>
            <td>${e.wedlock}</td>
        <tr/>
    </c:forEach>
</table>
<div>
    <a href="/java_web/employeeList?pageIndex=1&pageSize=${pageUtils.pageSize}">首页</a>

    <c:if test="${pageUtils.pageIndex!=1}" var="bf" scope="request">
        <a href="/java_web/employeeList?pageIndex=${pageUtils.pageIndex-1}&pageSize=${pageUtils.pageSize}">上一页</a>
    </c:if>

    <c:forEach begin="${pageUtils.firstPage}" end="${pageUtils.lastPage}" var="n">
        <a href="/java_web/employeeList?pageIndex=${n}&pageSize=${pageUtils.pageSize}">${ n }</a>
    </c:forEach>

    <c:if test="${pageUtils.pageIndex!=pageUtils.lastPage}" var="be" scope="request">
        <a href="/java_web/employeeList?pageIndex=${pageUtils.pageIndex+1}&pageSize=${pageUtils.pageSize}">下一页</a>
    </c:if>


    <a href="/java_web/employeeList?pageIndex=${pageUtils.pageCount}&pageSize=${pageUtils.pageSize}">尾页</a>
</div>

</body>
</html>
