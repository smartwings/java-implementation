package org.example.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter("/*")
public class MyFilter implements Filter {
    private final static String LOGIN_JSP = "/java_web/login.jsp";
    private final static String LOGIN = "/java_web/login";
    private final static String LOCAL = "/java_web/";


    @Override
    public void destroy() {
    }


    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req1 = (HttpServletRequest) req;
        HttpServletResponse resp1 = (HttpServletResponse) resp;
        String uri = req1.getRequestURI();
        Object username = req1.getSession().getAttribute("username");
        if (null != username || LOGIN_JSP.equals(uri) || LOGIN.equals(uri)) {
            chain.doFilter(req, resp);
        } else {
            resp1.sendRedirect("login.jsp");
        }

    }


    @Override
    public void init(FilterConfig config) throws ServletException {

    }

}
