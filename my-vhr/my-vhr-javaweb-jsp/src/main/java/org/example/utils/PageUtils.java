package org.example.utils;

import lombok.Data;

import java.util.List;


@Data
public class PageUtils<T> {


    private final static int STANDARD = 6;


    private final static int STANDARD_LAST = 4;

    private final static int PAGE_LENGTH = 10;


    private int pageIndex;


    private int pageSize = 10;


    private int totalSize;


    private List<T> data;


    private int pageCount;


    private int firstPage;


    private int lastPage;


    public PageUtils() {
    }


    public PageUtils(int pageIndex, int pageSize, int totalSize, List<T> data) {
        this.pageIndex = pageIndex;
        if (pageSize > 0) {
            this.pageSize = pageSize;
        }
        this.totalSize = totalSize;
        this.data = data;
        this.pageCount = totalSize % pageSize == 0 ? totalSize / pageSize : totalSize / pageSize + 1;
        this.firstPage = pageIndex - 5;
        this.lastPage = pageIndex + 4;
        // 如果当前页码小于6，开始的显示
        if (pageIndex < STANDARD) {
            this.firstPage = 1;
            this.lastPage = 10;
        }
        //如果当前页码大于等于总页面-6，最后的显示
        if (pageIndex >= this.pageCount - STANDARD) {
            this.firstPage = this.pageCount - 9;
            this.lastPage = this.pageCount;
        }
        if (this.pageCount < PAGE_LENGTH) {
            this.firstPage = 1;
            this.lastPage = this.pageCount;
        }
    }
}
