package org.example.service.impl;

import org.example.dao.HrDao;
import org.example.dao.impl.HrDaoImpl;
import org.example.service.HrService;


public class HrServiceImpl implements HrService {


    private HrDao hr = new HrDaoImpl();


    @Override
    public String login(String username, String password) {
        if (null == username || null == password) {
            return "用户名或密码为空，请重新输入";
        } else {
            int num = hr.login(username, password);
            if (1 == num) {
                return "登录成功";
            }
            if (num > 1) {
                System.out.println("用户名为：" + username + "的信息条数过多，为" + num + "条，请删除或修改");
                return "数据错误，请联系管理员。";
            }
            if (num < 1) {
                return "用户名或密码错误";
            }
        }
        return "登录失败";
    }
}
