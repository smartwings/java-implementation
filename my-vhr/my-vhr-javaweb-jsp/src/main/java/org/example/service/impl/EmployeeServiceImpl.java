package org.example.service.impl;

import org.example.dao.EmployeeDao;
import org.example.dao.impl.EmployeeDaoImpl;
import org.example.entity.Employee;
import org.example.service.EmployeeService;

import java.util.List;


public class EmployeeServiceImpl implements EmployeeService {
    EmployeeDao employeeDao = new EmployeeDaoImpl();


    @Override
    public List<Employee> queryAll(int pageIndex, int pageSize) {
        return employeeDao.queryAll(pageIndex, pageSize);
    }


    @Override
    public Boolean addEmployee(Employee employee) {
        if (employeeDao.addEmployee(employee) > 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Boolean updateEmployee(Employee employee) {
        if (employeeDao.updateEmployee(employee) > 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Boolean deleteEmployee(Integer id) {
        if (employeeDao.deleteEmployee(id) > 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Employee queryEmployee(Integer id) {
        return employeeDao.queryEmployee(id);
    }


    @Override
    public int count() {
        return employeeDao.count();
    }


}
