package org.example.service;

import org.example.entity.Employee;

import java.util.List;


public interface EmployeeService {


    List<Employee> queryAll(int pageIndex, int pageSize);


    Boolean addEmployee(Employee employee);


    Boolean updateEmployee(Employee employee);


    Boolean deleteEmployee(Integer id);


    Employee queryEmployee(Integer id);


    int count();

}
