window.onload = function () {
    const queryAll = document.getElementById("queryAll");
    const addEmployee = document.getElementById("addEmployee");
    const updateEmployee = document.getElementById("updateEmployee");
    const deleteEmployee = document.getElementById("deleteEmployee");
    const exit = document.getElementById("exit");

    queryAll.onclick = function () {
        window.open("/java_web/employeeList.html", '_self')
    };
    addEmployee.onclick = function () {
        window.open("/java_web/addEmployee.html", '_self')
    };
    updateEmployee.onclick = function () {
        window.open("/java_web/updateEmployee.html", '_self')
    };
    deleteEmployee.onclick = function () {
        window.open("/java_web/deleteEmployee.html", '_self')
    };
    exit.onclick = function () {
        window.open("/java_web/login.html", '_self')
    };

}