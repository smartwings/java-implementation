package org.example.mapper;

import org.apache.ibatis.annotations.Param;
import org.example.entity.Hr;


public interface HrMapper {


    Hr login(@Param("username") String username, @Param("password") String password);
}
