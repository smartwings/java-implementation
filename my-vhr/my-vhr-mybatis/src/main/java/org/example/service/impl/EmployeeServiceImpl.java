package org.example.service.impl;

import org.apache.ibatis.session.SqlSession;
import org.example.entity.Employee;
import org.example.mapper.EmployeeMapper;
import org.example.service.EmployeeService;
import org.example.utils.SqlSessionUtil;

import java.util.List;


public class EmployeeServiceImpl implements EmployeeService {
    SqlSession session = SqlSessionUtil.getSession();
    EmployeeMapper mapper = session.getMapper(EmployeeMapper.class);


    @Override
    public List<Employee> queryAll() {
        List<Employee> employees = mapper.selectAll();
        return employees;
    }


    @Override
    public Boolean addEmployee(Employee employee) {
        int num = mapper.addEmployee(employee);
        if (num > 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Boolean updateEmployee(Employee employee) {
        int num = mapper.updateEmployee(employee);
        if (num > 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Boolean deleteEmployee(Integer id) {
        int num = mapper.deleteEmployee(id);
        if (num > 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Employee queryEmployee(Integer id) {
        Employee employee = mapper.queryEmployee(id);
        return employee;
    }


    @Override
    public int count() {
        int num = mapper.count();
        return num;
    }


}
