package org.example.service.impl;


import org.apache.ibatis.session.SqlSession;
import org.example.mapper.HrMapper;
import org.example.service.HrService;
import org.example.utils.SqlSessionUtil;


public class HrServiceImpl implements HrService {


    SqlSession session = SqlSessionUtil.getSession();
    HrMapper mapper = session.getMapper(HrMapper.class);


    @Override
    public String login(String username, String password) {
        if (null == username || null == password) {
            return "用户名或密码为空，请重新输入";
        } else {
            if (null != mapper.login(username, password)) {
                return "登录成功";
            } else {
                return "登录失败";
            }
        }
    }
}
