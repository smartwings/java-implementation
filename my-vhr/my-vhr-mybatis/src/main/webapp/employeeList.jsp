<%--
  Created by IntelliJ IDEA.
  User: 34186
  Date: 2022/8/26
  Time: 19:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>员工列表</title>
</head>
<body>
<a href="welcome.jsp">返回欢迎页面</a><br/>
<table border="1">
    <tr>
        <th>工号</th>
        <th>名字</th>
        <th>性别</th>
        <th>生日</th>
        <th>结婚状态</th>
        <th>操作</th>
    </tr>
    <c:forEach items="${pageInfo.list}" var="e">
        <tr>
            <td>${e.id}</td>
            <td>${e.name}</td>
            <td>${e.gender}</td>
            <td>${e.birthday}</td>
            <td>${e.wedlock}</td>
            <td><a href="/mybatis/deleteEmployee?id=${e.id}">删除</a></td>
        <tr/>
    </c:forEach>
</table>
<div>
    <a href="/mybatis/employeeList?pageNum=1&pageSize=${pageInfo.pageSize}">首页</a>

    <c:if test="${!pageInfo.isFirstPage}" var="bf" scope="request">
        <a href="/mybatis/employeeList?pageNum=${pageInfo.pageNum-1}&pageSize=${pageInfo.pageSize}">上一页</a>
    </c:if>

    <c:forEach begin="${pageInfo.navigateFirstPage}" end="${pageInfo.navigateLastPage}" var="n">
        <a href="/mybatis/employeeList?pageNum=${n}&pageSize=${pageInfo.pageSize}">${ n }</a>
    </c:forEach>

    <c:if test="${!pageInfo.isLastPage}" var="be" scope="request">
        <a href="/mybatis/employeeList?pageNum=${pageInfo.pageNum+1}&pageSize=${pageInfo.pageSize}">下一页</a>
    </c:if>


    <a href="/mybatis/employeeList?pageNum=${pageInfo.pages}&pageSize=${pageInfo.pageSize}">尾页</a>
</div>

</body>
</html>
