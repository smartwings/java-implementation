window.onload = function () {
    const queryAll = document.getElementById("queryAll");
    const addEmployee = document.getElementById("addEmployee");
    const updateEmployee = document.getElementById("updateEmployee");
    const exit = document.getElementById("exit");

    queryAll.onclick = function () {
        window.open("/mybatis/employeeList?pageNum=1&pageSize=10", '_self')
    };
    addEmployee.onclick = function () {
        window.open("/mybatis/addEmployee.jsp", '_self')
    };
    updateEmployee.onclick = function () {
        window.open("/mybatis/updateEmployee.jsp", '_self')
    };
    exit.onclick = function () {
        window.open("/mybatis/login.jsp", '_self')
    };

}