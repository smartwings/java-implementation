window.onload = function () {
    const queryAll = document.getElementById("queryAll");
    const addEmployee = document.getElementById("addEmployee");
    const updateEmployee = document.getElementById("updateEmployee");
    const upload = document.getElementById("upload");
    const exit = document.getElementById("exit");
    const ajaxWelcome = document.getElementById("ajaxWelcome");
    const restWelcome = document.getElementById("restWelcome");

    queryAll.onclick = function () {
        window.open("employeeList?pageNum=1&pageSize=10", '_self')
    };
    addEmployee.onclick = function () {
        window.open("addEmployeeView", '_self')
    };
    updateEmployee.onclick = function () {
        window.open("updateEmployeeView", '_self')
    };
    upload.onclick = function () {
        window.open("uploadView", '_self')
    };
    exit.onclick = function () {
        window.open("loginView", '_self')
    };
    ajaxWelcome.onclick = function () {
        window.open("ajaxWelcomeView", '_self')
    };
    restWelcome.onclick = function () {
        window.open("restWelcomeView", '_self')
    }

}