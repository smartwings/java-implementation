window.onload = function () {
    const queryAll = document.getElementById("queryAll");
    const exit = document.getElementById("exit");
    const welcome = document.getElementById("welcome");
    const restWelcome = document.getElementById("restWelcome");

    queryAll.onclick = function () {
        window.open("employeeListView_ajax", '_self')
    };
    exit.onclick = function () {
        window.open("loginView", '_self')
    };
    welcome.onclick = function () {
        window.open("welcomeView", '_self')
    };
    restWelcome.onclick = function () {
        window.open("restWelcomeView", '_self')
    }

}