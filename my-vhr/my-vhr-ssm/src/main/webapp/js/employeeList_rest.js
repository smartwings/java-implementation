window.onload = function () {
    const queryAll = document.getElementById("queryAll");
    const exit = document.getElementById("exit");
    const ajaxWelcome = document.getElementById("ajaxWelcome");
    const welcome = document.getElementById("welcome");

    queryAll.onclick = function () {
        window.open("employeeListView_rest", '_self')
    };
    exit.onclick = function () {
        window.open("loginView", '_self')
    };
    ajaxWelcome.onclick = function () {
        window.open("ajaxWelcomeView", '_self')
    };
    welcome.onclick = function () {
        window.open("welcomeView", '_self')
    }

}