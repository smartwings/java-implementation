<%--
  Created by IntelliJ IDEA.
  User: 34186
  Date: 2022/8/26
  Time: 19:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <title>员工列表</title>
    <style>
        body {
            text-align: center;
        }

        table {
            margin: auto;
        }

        td {
            text-align: center;
        }
    </style>
    <script type="text/javascript" src="webjars/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function employeeLists(pageNum, pageSize) {
            $.ajax({
                url: "/employeeList_rest/" + pageNum + "/" + pageSize,//请求名称
                type: "get",
                async: true,//异步请求，默认就是异步请求
                dataType: "json",//如果返回的是简单类型，添加text，如果返回的是对象，集合也就是json数据
                success: function (data) {
                    console.log(data)
                    let i;
                    var head = "<tr>\n" +
                        "        <th>工号</th>\n" +
                        "        <th>名字</th>\n" +
                        "        <th>性别</th>\n" +
                        "        <th>生日</th>\n" +
                        "        <th>结婚状态</th>\n" +
                        "        <th>操作</th>\n" +
                        "    </tr>"
                    const employees = data.list;
                    for (let i = 0; i < employees.length; i++) {
                        head += "<tr>\n" +
                            "        <td>" + employees[i].id + "</td>\n" +
                            "        <td>" + employees[i].name + "</td>\n" +
                            "        <td>" + employees[i].gender + "</td>\n" +
                            "        <td>" + employees[i].birthday + "</td>\n" +
                            "        <td>" + employees[i].wedlock + "</td>\n" +
                            "        <td><a href='#'onclick='employeeDelete(" + employees[i].id + "," + pageNum + "," + pageSize + ")'>删除</a></td>\n" +
                            "        <tr/>"
                    }
                    $("#employeeList").html(head)

                    let page = "<a href='#'onclick='employeeLists(1," + data.pageSize + ")'>首页</a>&nbsp;&nbsp;";
                    if (!data.isFirstPage) {
                        page += "<a href='#'onclick='employeeLists(" + (data.pageNum - 1) + "," + data.pageSize + ")'>上一页</a>&nbsp;&nbsp;"
                    }
                    for (i = data.navigateFirstPage; i <= data.navigateLastPage; i++) {
                        page += "<a href='#'onclick='employeeLists(" + i + "," + data.pageSize + ")'>  " + i + "  </a>&nbsp;&nbsp;"
                    }
                    if (!data.isLastPage) {
                        page += "<a href='#'onclick='employeeLists(" + (data.pageNum + 1) + "," + data.pageSize + ")'>下一页</a>&nbsp;&nbsp;"
                    }
                    page += "<a href='#'onclick='employeeLists(" + data.pages + "," + data.pageSize + ")'>尾页</a>【" + data.pageNum + "/" + data.pages + "】"
                    const obj = document.getElementsByTagName("option");
                    for (i = 0; i < obj.length; i++) {
                        if (obj[i].value == data.pageSize) {
                            obj[i].selected = true;
                        }
                    }
                    $("#page").html(page)
                    $(".pageSize").change(function () {
                        var pageSize = $(".pageSize").val()
                        employeeLists(pageNum, pageSize)
                    })
                }

            })
        }

        function employeeDelete(id, pageNum, pageSize) {
            $.ajax({
                url: "/employeeList_rest/" + id,
                type: "delete",
                async: true,//异步请求，默认就是异步请求
                dataType: "json",//如果返回的是简单类型，添加text，如果返回的是对象，集合也就是json数据
                success: function (data) {
                    if (data.success) {
                        employeeLists(pageNum, pageSize)
                    } else {
                        alert(data.msg)
                    }
                }
            })
        }

        $(function (pageNum, pageSize) {
            console.log(pageNum, pageSize)
            if (pageNum == null || pageSize == null) {
                employeeLists(1, 10)
            } else {
                employeeLists(pageNum, pageSize)
            }

        });
    </script>
</head>
<body>
<a href="/welcomeView">返回欢迎页面</a><br/>
<table border="1" id="employeeList" name="employeeList" width="100%">
</table>
<div>
<span id="page">
</span>
    <span>
        <select class="pageSize">
             <option value="5">5</option>
             <option value="10">10</option>
             <option value="15">15</option>
             <option value="20">20</option>
        </select>
    </span>
</div>
</body>
</html>
