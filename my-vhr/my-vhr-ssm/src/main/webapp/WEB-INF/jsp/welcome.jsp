<%--
  Created by IntelliJ IDEA.
  User: 34186
  Date: 2022/8/26
  Time: 17:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>欢迎页面</title>
    <script type="text/javascript" src="../../js/employeeList.js"></script>
</head>
<body>
欢迎使用员工管理系统
<input type="button" id="queryAll" value="查询全部员工信息">
<input type="button" id="addEmployee" value="添加员工信息">
<input type="button" id="updateEmployee" value="修改员工信息">
<input type="button" id="upload" value="文件上传">
<input type="button" id="exit" value="退出系统"><br>
<input type="button" id="ajaxWelcome" value="ajax欢迎页面">
<input type="button" id="restWelcome" value="rest欢迎页面">
</body>
</html>
