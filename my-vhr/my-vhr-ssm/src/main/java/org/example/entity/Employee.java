package org.example.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


public class Employee implements Serializable {


    private Integer id;


    private String name;


    private String gender;


    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;


    private String wedlock;


    public Employee() {
    }


    public Employee(Integer id, String name, String gender, Date birthday, String wedlock) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.birthday = birthday;
        this.wedlock = wedlock;
    }


    public Employee(String name, String gender, Date birthday, String wedlock) {
        this.name = name;
        this.gender = gender;
        this.birthday = birthday;
        this.wedlock = wedlock;
    }


    @Override
    public String toString() {
        return id + "\t\t\t" + String.format("%-8s", name) + "\t" + String.format("%-8s", gender) + String.format("%-8s", birthday) + "\t\t\t" + String.format("%-8s", wedlock);
    }


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getName() {
        String username = this.name;
        return username;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getGender() {
        return gender;
    }


    public void setGender(String gender) {
        this.gender = gender;
    }


    public Date getBirthday() {
        return birthday;
    }


    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }


    public String getWedlock() {
        return wedlock;
    }


    public void setWedlock(String wedlock) {
        this.wedlock = wedlock;
    }
}
