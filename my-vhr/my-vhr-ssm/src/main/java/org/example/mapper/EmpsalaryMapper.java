package org.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.example.entity.Empsalary;


@Mapper
public interface EmpsalaryMapper {


    int deleteByPrimaryKey(Integer id);


    int insert(Empsalary record);


    int insertSelective(Empsalary record);


    Empsalary selectByPrimaryKey(Integer id);


    int updateByPrimaryKeySelective(Empsalary record);


    int updateByPrimaryKey(Empsalary record);


    int deleteByForeignKey(int id);
}