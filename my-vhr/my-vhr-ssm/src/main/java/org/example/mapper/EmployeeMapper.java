package org.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.example.entity.Employee;

import java.util.List;


@Mapper
public interface EmployeeMapper {


    int addEmployee(Employee employee);


    int updateEmployee(Employee employee);


    int deleteEmployee(int id);


    Employee queryEmployee(int id);


    int count();


    List<Employee> selectAll();
}
