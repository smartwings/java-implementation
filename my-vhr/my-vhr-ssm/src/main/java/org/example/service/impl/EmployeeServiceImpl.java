package org.example.service.impl;

import org.example.entity.Employee;
import org.example.mapper.EmployeeMapper;
import org.example.mapper.EmpsalaryMapper;
import org.example.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional(rollbackFor = Exception.class)
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeMapper mapper;
    @Autowired
    private EmpsalaryMapper empsalaryMapper;


    @Override
    public List<Employee> queryAll() {
        List<Employee> employees = mapper.selectAll();
        return employees;
    }


    @Override
    public Boolean addEmployee(Employee employee) {
        int num = mapper.addEmployee(employee);
        if (num > 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Boolean updateEmployee(Employee employee) {
        int num = mapper.updateEmployee(employee);
        if (num > 0) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public Boolean deleteEmployee(Integer id) {
        int num1 = empsalaryMapper.deleteByForeignKey(id);
        if (num1 > 0) {
            int num = mapper.deleteEmployee(id);
            if (num > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    @Override
    public Employee queryEmployee(Integer id) {
        Employee employee = mapper.queryEmployee(id);
        return employee;
    }


    @Override
    public int count() {
        int num = mapper.count();
        return num;
    }


}
