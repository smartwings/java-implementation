package org.example.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.example.entity.Employee;
import org.example.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class AjaxEmployeeController {
    @Autowired
    private EmployeeService employeeService;


    @RequestMapping("employeeListView_ajax")
    public String employeeListView() {
        return "employeeList_ajax";
    }


    @RequestMapping("employeeList_ajax")
    @ResponseBody
    public PageInfo employeeListAjax(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Employee> employees = employeeService.queryAll();
        PageInfo pageInfo = new PageInfo(employees, 10);
        return pageInfo;
    }
}
