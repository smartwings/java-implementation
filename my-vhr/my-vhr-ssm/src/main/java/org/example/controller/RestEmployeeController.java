package org.example.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.example.entity.Employee;
import org.example.service.EmployeeService;
import org.example.service.EmpsalaryService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("employeeList_rest")
public class RestEmployeeController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmpsalaryService empsalaryService;


    @GetMapping("/{pageNum}/{pageSize}")
    public PageInfo employeeListRest(@PathVariable int pageNum, @PathVariable int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Employee> employees = employeeService.queryAll();
        PageInfo pageInfo = new PageInfo(employees, 10);
        return pageInfo;
    }

    @PostMapping
    public Map addEmployee(Employee employee) {
        Boolean isSuccess = employeeService.addEmployee(employee);
        return getWelcomeView(isSuccess);
    }

    @PutMapping
    public Map updateEmployee(Employee employee) {
        Boolean isSuccess = employeeService.updateEmployee(employee);
        return getWelcomeView(isSuccess);
    }

    @DeleteMapping("/{id}")
    public Map deleteEmployee(@PathVariable int id) {
        Boolean isSuccess = employeeService.deleteEmployee(id);
        return getWelcomeView(isSuccess);
    }


    private Map getWelcomeView(Boolean isSuccess) {
        if (isSuccess) {
            return new HashMap() {
                {
                    put("success", true);
                    put("msg", "操作成功");
                }
            };
        } else {
            return new HashMap() {
                {
                    put("success", false);
                    put("msg", "操作失败");
                }
            };
        }
    }
}
