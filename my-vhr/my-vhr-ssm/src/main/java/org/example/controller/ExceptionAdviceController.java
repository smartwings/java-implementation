package org.example.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionAdviceController {
    @ExceptionHandler(ArithmeticException.class)
    public String arithmeticException(Exception e) {
        e.printStackTrace();
        System.out.println("数学运算异常...");
        return "error";
    }

    @ExceptionHandler(NullPointerException.class)
    public String nullPointerException(Exception e, HttpServletRequest request) {
        e.printStackTrace();
        System.out.println("空指针异常...");
        return "error";
    }

    // 其它异常处理
    @ExceptionHandler(Exception.class)
    public String exception(Exception e, HttpServletRequest request) {
        e.printStackTrace();
        return "error";
    }

}